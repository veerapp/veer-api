moment = require "moment"
mongoose = require "mongoose"
Schema = mongoose.Schema
common = require "./common"

GarageException = do ->

  GarageExceptionSchema = new Schema(

    active:
      type: Boolean
      default: true

    name:
      type: String
      default: null

    description:
      type: String
      default: null

    date:
      start:
        type: Date
        default: null
      stop:
        type: Date
        default: null

    created:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()
  )
  
  GarageExceptionSchema.pre "save", (next) ->
    @lastmod = moment().utc().format()
    next()

  schema: GarageExceptionSchema
  model: common.getOrCreateModel "GarageException", GarageExceptionSchema

module.exports = GarageException
