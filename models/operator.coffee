moment = require "moment"
mongoose = require "mongoose"
paginate = require 'mongoose-paginate'
Address = require './address'
Picture = require './picture'
Schema = mongoose.Schema
common = require "./common"

Operator = do ->

  OperatorSchema = new Schema(

    active:
      type: Boolean
      default: true

    name:
      type: String
      default: null

    phone:
      type: String
      default: null

    web:
      type: String
      default: null

    logo_url:
      type: String
      default:  null

    geo:
      type: [ Number ]
      index: "2d"

    address: [ Address.schema ]

    pictures: [ Picture.schema ]

    deleted:
      type: Boolean
      default: false

    created:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()
  )
  
  OperatorSchema.pre "save", (next) ->
    @lastmod = moment().utc().format()
    next()

  OperatorSchema.plugin(paginate)
  
  schema: OperatorSchema
  model: common.getOrCreateModel "Operator", OperatorSchema

module.exports = Operator
