moment = require "moment"
mongoose = require "mongoose"
paginate = require 'mongoose-paginate'
Address = require './address'
Picture = require './picture'
Schema = mongoose.Schema
common = require "./common"

Venue = do ->

  VenueSchema = new Schema(

    active:
      type: Boolean
      default: true

    name:
      type: String
      default: null

    phone:
      type: String
      default: null

    web:
      type: String
      default: null

    logo_url:
      type: String
      default:  null

    geo:
      type: [ Number ]
      index: "2d"

    address: [ Address.schema ]

    pictures: [ Picture.schema ]

    deleted:
      type: Boolean
      default: false

    created:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()
  )
  
  VenueSchema.pre "save", (next) ->
    @lastmod = moment().utc().format()
    next()

  VenueSchema.plugin(paginate)
  
  schema: VenueSchema
  model: common.getOrCreateModel "Venue", VenueSchema

module.exports = Venue
