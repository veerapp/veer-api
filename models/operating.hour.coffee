moment = require "moment"
mongoose = require "mongoose"
paginate = require 'mongoose-paginate'
Schema = mongoose.Schema
common = require "./common"

OperatingHour = do ->

  OperatingHourSchema = new Schema(

    active:
      type: Boolean
      default: true

    schedule:
      monday:
        type: Boolean
        default: false

      tuesday:
        type: Boolean
        default: false

      wednesday:
        type: Boolean
        default: false

      thursday:
        type: Boolean
        default: false

      friday:
        type: Boolean
        default: false

      saturday:
        type: Boolean
        default: false

      sunday:
        type: Boolean
        default: false

    open:
      type: String
      default: null

    closed:
      type: String
      default: false

    created:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()
  )
  
  OperatingHourSchema.pre "save", (next) ->
    @lastmod = moment().utc().format()
    next()

  OperatingHourSchema.plugin(paginate)
  
  schema: OperatingHourSchema
  model: common.getOrCreateModel "OperatingHour", OperatingHourSchema

module.exports = OperatingHour
