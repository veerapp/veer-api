 

# internal modules
moment        = require "moment"
mongoose      = require "mongoose"
common = require "./common"
# validator     = require "mongoose-validator"

# locally modules
RegionModel   = require "./region"
AddressModel  = require "./address"

# model definition
Location = do ->
  Schema = mongoose.Schema

  LocationSchema = new Schema(
    geo:        
      type: [ Number ]
      index: "2d"

    address: [ AddressModel.schema ]
    
    region: [ RegionModel.schema ]

    altitude:
      type: Number
      default: null

    speed:
      type: Number
      default: null
      
    accuracy:
      type: Number
      default: null

    active:
      type: Boolean
      default: false

    public:
      type: Boolean
      default: true

    created:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()
  )
  
  LocationSchema.pre "save", (next) ->
    @lastmod = moment().utc().format()
    next()

  LocationSchema.index "geo": "2d"
  
  _model = common.getOrCreateModel "locations", LocationSchema

  schema: LocationSchema
  model: _model

module.exports = Location
