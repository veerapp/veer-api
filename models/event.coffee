mongoose = require 'mongoose'
paginate = require 'mongoose-paginate'
Schema = mongoose.Schema
common = require "./common"
moment = require "moment"
Address = require "./address"

Event = do ->

  EventSchema = new Schema(

    name:
      type: String
      required: true

    description:
      type: String
      default: null

    web:
      type: String
      default: null

    phone:
      type: String
      default: null

    address: [ Address.schema ]

    start_time:
      type: Date
      required: true

    stop_time:
      type: Date
      required: true

    created:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()

  )

  EventSchema.pre "save", (next) ->
    @lastmod = moment().utc().format()
    next()

  EventSchema.plugin(paginate)

  schema: EventSchema
  model: common.getOrCreateModel('Event', EventSchema)


module.exports = Event