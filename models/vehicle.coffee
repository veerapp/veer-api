moment = require "moment"
mongoose = require "mongoose"
paginate = require 'mongoose-paginate'
Schema = mongoose.Schema
common = require "./common"

Vehicle = do ->

  VehicleSchema = new Schema(

    make:
      type: String
      default: null

    model:
      type: String
      default: null

    year: 
      type: String
      default: null

    vin:
      type: String
      default: null

    plate: 
      type: String
      default: null

    color: 
      type: String
      default: null

    created:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()
  )
  
  VehicleSchema.pre "save", (next) ->
    @lastmod = moment().utc().format()
    next()

  VehicleSchema.plugin(paginate)
  
  schema: VehicleSchema
  model: common.getOrCreateModel "Vehicle", VehicleSchema

module.exports = Vehicle
