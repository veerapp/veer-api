mongoose = require 'mongoose'
Schema = mongoose.Schema

moment = require "moment"
common = require "../common"

Twitter = do ->

  TwitterSchema = new Schema(

    url:
      type: String
      default: null

    created:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()

  )

  schema: TwitterSchema
  model: common.getOrCreateModel('Twitter', TwitterSchema)

module.exports = Twitter