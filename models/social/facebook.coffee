mongoose = require 'mongoose'
Schema = mongoose.Schema

moment = require "moment"
common = require "../common"

Facebook = do ->

  FacebookSchema = new Schema(

    id:
      type: String
      default: null

    access_token:
      type: String
      default: null

    refresh_token:
      type: String
      default: null

    url:
      type: String
      default: null

    created:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()

  )

  schema: FacebookSchema
  model: common.getOrCreateModel('Facebook', FacebookSchema)

module.exports = Facebook