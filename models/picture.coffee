moment = require "moment"
mongoose = require "mongoose"
paginate = require 'mongoose-paginate'
Schema = mongoose.Schema
common = require "./common"

Picture = do ->

  PictureSchema = new Schema(

    id: 
      type: String
      default: null

    url: 
      type: String
      default: null

    caption:
      type: String
      default: null

    created:
      type: Date
      default: null

    lastmod:
      type: Date
      default: null
  )
  
  PictureSchema.pre "save", (next) ->
    @lastmod = moment().utc().format()
    next()

  PictureSchema.plugin(paginate)
  
  schema: PictureSchema
  model: common.getOrCreateModel "Picture", PictureSchema

module.exports = Picture
