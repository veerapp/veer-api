mongoose = require 'mongoose'
paginate = require 'mongoose-paginate'
Schema = mongoose.Schema

moment = require "moment"
common = require "./common"

Region = do ->

  RegionSchema = new Schema(

    formatted:
      type: String
      default: null

    city:
      type: String

    state:
      type: String

    active:
      type: Boolean
      default: false

    img:
      type: String
      default: null

    slug: 
      type: String
      default: null

    geo:        
      type: [ Number ]
      index: "2d"

    created:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()

  )

  RegionSchema.pre "save", (next) ->
    @lastmod = moment().utc().format()
    next()

  RegionSchema.plugin(paginate)
  
  schema: RegionSchema
  model: common.getOrCreateModel("Region", RegionSchema)

module.exports = Region