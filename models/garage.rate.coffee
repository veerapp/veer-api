moment = require "moment"
mongoose = require "mongoose"
Schema = mongoose.Schema
common = require "./common"

GarageRate = do ->

  GarageRateSchema = new Schema(

    # defines if this rate is active
    active:
      type: Boolean
      default: true

    schedule:
      monday: 
        type: Boolean
        default: true
      tuesday:
        type: Boolean
        default: true
      wednesday:
        type: Boolean
        default: true
      thursday:
        type: Boolean
        default: true
      friday:
        type: Boolean
        default: true
      saturday:
        type: Boolean
        default: true
      sunday:
        type: Boolean
        default: true

      # values: weekdays (mon-fri), weekends (sat-sun), every_day (mon-sun), monday, tuesday, wednesday, thursday, friday, saturday, sunday
      # type: String
      # enum: ['weekdays', 'weekends', 'every_day', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
      # default: null

    # values: early-bird, daily, evening, flat
    # period:
    #   type: String
    #   enum: ['early_bird', 'daily', 'evening', 'flat']
    #   default: null

    ###
      we have two different rate types, the 'range' and the 'each'. 

      range - handles cases where there is a fixed (bounded) amount of time mapped to a given cost
      each - handles cases where there is no upper bound but there is an recurring interval where one is charge a fixed cost
    ###
    type:

      range_hour:
        active: 
          type: Boolean
          default: false
        ###
          greater than hour & less than hour should be in 2400 time. This defines the valid duration bound of this rate in HOURS. (RANGE)
          examples: 
          1- From 0800 hours to 1200 hours charge $X
          2- From 0500 hours to 0700 hours charge $Y
          3- From 1500 hours to 1800 hours charge $Z
        ###
        gt:
          type: String
          default: null

        lt:
          type: String
          default: null

      range_min:
        active: 
          type: Boolean
          default: false
        ###
          greater than minute & less than minute should be an integer. This defines the valid duration bound of this rate in MINUTES. (RANGE)
          examples:
          1- From 0 minutes to 60 minutes charge $X
          2- From 60 minutes to 180 minutes charge $Y
          3- From 180 minutes to 360 minutes charge $Z
        ###
        gt:
          type: String
          default: null
        lt:
          type: String
          default: null

      each:
        active: 
          type: Boolean
          default: false

        # Each 30 minutes costs $X
        minute:
          type: Number
          default: null

      # operating hours are taken as the bounds for flat rate
      flat:
        active:
          type: Boolean
          default: false

      early_bird:
        active:
          type: Boolean
          default: false

        start_gt:
          type: String
          default: null

        start_lt:
          type: String
          default: null

        stop_gt:
          type: String
          default: null

        stop_lt:
          type: String
          default: null

      event:
        active:
          type: Boolean
          default: false

        name:
          type: String
          default: null

        description:
          type: String
          default: null

        gt:
          type: String
          default: null

        lt: 
          type: String
          default: null

    # the cost associates to this rate
    cost:
      type: Number
      default: null

    currency:
      type: String
      default: "dollars"

    date:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()
  )
  
  GarageRateSchema.pre "save", (next) ->
    @lastmod = moment().utc().format()
    next()

  schema: GarageRateSchema
  model: common.getOrCreateModel "GarageRate", GarageRateSchema

module.exports = GarageRate
