mongoose = require 'mongoose'
Schema = mongoose.Schema
common = require "./common"
moment = require "moment"

Address = do ->

  AddressSchema = new Schema(

    number: 
      type: String
      default: null

    street:
      type: String
      default: null

    city:
      type: String
      default: null

    state:
      type: String
      default: null

    zip:
      type: String
      default: null

    country:
      type: String
      default: null

    countryCode:
      type: String
      default: null

    geo:
      type: [ Number ]
      index: "2d"

    override:
      type: Boolean
      default: false

    created:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()

  )

  AddressSchema.pre "save", (next) ->
    @lastmod = moment().utc().format()
    next()

  schema: AddressSchema
  model: common.getOrCreateModel('Address', AddressSchema)

module.exports = Address