mongoose = require 'mongoose'
paginate = require 'mongoose-paginate'

Schema = mongoose.Schema

common = require "./common"
moment = require "moment"
Address = require "./address"
Picture = require "./picture"
Region = require "./region"
GarageRate = require "./garage.rate"
GarageException = require "./garage.exception"
OperatingHour = require "./operating.hour"

Garage = do ->

  GarageSchema = new Schema(

    name: 
      type: String
      required: true

    active:
      type: Boolean
      default: true

    logo_url:
      type: String
      default: null

    rate_card_pic_id:
      type: String
      default: null

    entrance_pic_id:
      type: String
      default: null

    # picture_url:
    #   type: String
    #   default: null

    event:

      # default event cost for garage
      cost:
        type: Number
        default: null

      # start buffer
      start:
        type: Number
        default: null

      # stop buffer
      stop:
        type: Number
        default: null

    web:
      type: String
      default: null

    email:
      type: String
      default: null

    num_spaces:
      type: Number
      default: null

    deleted:
      type: Boolean
      default: false

    phone: 
      type: String
      default: null

    # type:
    #   type: String
    #   enum: ['aboveground', 'surface', 'underground']
    #   default: null

    max_height:
      type: Number
      default: null

    # pictures: [ Picture.schema ]
    # pictures: [ mongoose.Schema.Mixed ]

    operator_id:
      type: Schema.ObjectId
      ref: "Operator"
      default: null

    payment:
      cash:
        type: Boolean
        default: false

      check:
        type: Boolean
        default: false

      credit:
        type: Boolean
        default: false

    properties:
      security_cameras:
        type: Boolean
        default: false

      elevators:
        type: Boolean
        default: false

      wheelchair:
        type: Boolean
        default: false

      ev_charger:
        type: Boolean
        default: false

      wifi_zone:
        type: Boolean
        default: false

    services:
      car_wash:
        type: Boolean
        default: false

      gas_fillup:
        type: Boolean
        default: false

      security_escort:
        type: Boolean
        default: false

      flat_tire_assistance:
        type: Boolean
        default: false

      valet:
        type: Boolean
        default: false

    address: [ Address.schema ]

    region: [ Region.schema ]

    rates: [ GarageRate.schema ]

    exceptions: [ GarageException.schema ]

    hours: [ OperatingHour.schema ]

    venues: [ type : Schema.ObjectId, ref : 'Venue' ]

    created:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()

  )

  GarageSchema.pre "save", (next) ->
    @lastmod = moment().utc().format()
    next()

  GarageSchema.index "geo": "2d"

  GarageSchema.plugin(paginate)
  
  schema: GarageSchema
  model: common.getOrCreateModel('Garage', GarageSchema)

module.exports = Garage