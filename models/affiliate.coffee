mongoose = require 'mongoose'
paginate = require 'mongoose-paginate'
Schema = mongoose.Schema
common = require "./common"

moment = require "moment"
Address = require "./address"

Affiliate = do ->

  AffiliateSchema = new Schema(

    name:
      type: String
      required: true

    web:
      type: String
      default: null

    phone:
      type: String
      default: null

    description:
      type: String
      default: null

    address: [ Address.schema ]

    created:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()

  )

  AffiliateSchema.pre "save", (next) ->
    @lastmod = moment().utc().format()
    next()

  AffiliateSchema.plugin(paginate)

  schema: AffiliateSchema
  model: common.getOrCreateModel('Affiliate', AffiliateSchema)

module.exports = Affiliate