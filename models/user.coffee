passwordHash = require 'password-hash'
mongoose = require 'mongoose'
paginate = require 'mongoose-paginate'
Schema = mongoose.Schema

Facebook = require "./social/facebook"
Twitter = require "./social/twitter"
Vehicle = require "./vehicle"
common = require "./common"

moment = require "moment"
bcrypt = require "bcrypt"

User = do ->

  UserSchema = new Schema(
    
    active:
      type: Boolean
      default: true

    firstname:
      type: String
      default: null

    lastname:
      type: String
      default: null

    phone: 
      type: String

    email:
      type: String
      index:
        unique: true
      required: true

    password:
      type: String
      required: true

    # hashed_password:
    #   type: String
    #   required: true

    deleted:
      type: Boolean
      default: false

    token:
      type: String
      default: null

    accept_terms:
      type: Boolean
      default: false

    is_admin:
      type: Boolean
      default: false

    facebook: [ Facebook.schema ]

    twitter: [ Twitter.schema ]

    vehicles: [ Vehicle.schema ]

    created:
      type: Date
      default: moment().utc().format()

    lastmod:
      type: Date
      default: moment().utc().format()

  )

  UserSchema.pre "save", (next) ->
    @lastmod = moment().utc().format()
    next()

  # UserSchema.virtual('password').set (password) ->
  #   options =
  #     algorithm: 'sha256',
  #     iterations: 1024,
  #     saltLength: 10
  #   @hashed_password = passwordHash.generate password, options

  # UserSchema.methods.authenticate = (plainText) ->
  #   passwordHash.verify(plainText, @hashed_password)

  UserSchema.plugin(paginate)
  
  schema: UserSchema
  model: common.getOrCreateModel('User', UserSchema)

module.exports = User