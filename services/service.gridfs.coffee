mongoose = require "mongoose"
request  = require "request"
Grid = require "gridfs-stream"
logger = require '../config/logger'
_ = require "lodash"
fs = require "fs"

module.exports =

  getFile: (id, res, cb) ->
    GFS = Grid(mongoose.connection.db, mongoose.mongo)
    store = GFS.createReadStream(_id: id)
    res.set 'Content-Type': 'image/png'
    store.pipe res

  createFile: (req, cb) ->
    GFS = Grid(mongoose.connection.db, mongoose.mongo)
    # return cb null, null  if _.isUndefined(req.busboy)
    logger.info "creating new file in gridfs"

    req.pipe req.busboy

    req.busboy.on "file", (fieldname, file, filename, encoding, mimetype) ->
      logger.info "saving file " + filename
      store = GFS.createWriteStream(
        filename: filename
        content_type: mimetype
      )
      file.pipe store
      store.on "close", (file) ->
        logger.info "successfully saved file " + JSON.stringify(file, null, 2)
        cb null, file
      return

    # Stream file here
    req.busboy.on "finish", ->
      logger.info "finished gridfs upload"

  deleteFile: (id, cb) ->
    GFS = Grid(mongoose.connection.db, mongoose.mongo)
    logger.info "deleting file [" + id + "] in gridfs"
    GFS.exist _id: id, (err, found) ->
      if found
        GFS.remove _id: id, (err) -> 
          logger.err "error deleting file: " + err if err
          return cb err if err
          cb null, true

  updateFile: (id, req, cb) ->
    GFS = Grid(mongoose.connection.db, mongoose.mongo)
    logger.info "updating file [" + id + "] in gridfs"
    return cb null, null  if _.isEqual(id, "null")
    @createFile req, (err, file) =>
      logger.err "error updating file: " + err if err
      return cb err if err
      @deleteFile id, (err, deletedFile) =>
        cb null, file


