moment = require 'moment'
validator = require 'validator'
logger = require '../config/logger'

Garage = require '../models/garage'
Address = require '../models/address'
Operator = require '../models/operator'
gridfs_service = require '../services/service.gridfs'

_ = require 'lodash'

module.exports =

  getGarages: (query, cb) ->
    query = _.defaults query,
      page: 1
      per_page: 5
      # distance: 0.7
      # latitude: 0
      # longitude: 0
      # timestamp: moment().utc().format()
      # page: 1
      # order: "desc"
    query = Garage.model.paginate {deleted: false}, query.page, query.per_page, (err, pageCount, paginatedResults, itemCount) ->
      return cb err if err
      data =
        page_count: pageCount
        total_count: itemCount
        items: paginatedResults
      cb null, data

      # "coordinates":
      #   $near: [ parseFloat(data.longitude), parseFloat(data.latitude) ]
        # $maxDistance: distance
    # query.limit data.limit
    # query.exec (err, garages) ->
    #   return cb err if err
    #   return cb null, [] if _.isEmpty garages
    #   cb null, garages

  getGarage: (id, cb) ->
    Garage.model.findOne {_id: id}, (err, garage) -> cb err, garage

  createGarage: (data, cb) ->
    data = _.defaults data,
      id: null
      name: null
      active: false
      hours: []
      event:
        cost: null
        start: null
        stop: null
      web: null
      email: null
      num_spaces: null
      phone: null
      max_height: null
      rate_card_pic_id: null
      entrance_pic_id: null
      logo_url: null
      operator_id: null
      geo: []
      payment:
        cash: null
        check: null
        credit: null
      properties:
        security_cameras: null
        elevators: null
        wheelchair: null
        ev_charger: null
        wifi_zone: null
      services:
        car_wash: null
        gas_fillup: null
        security_escort: null
        flat_tire_assistance: null
        valet: null
      address: []
      rates: []
      exceptions: []
    _validate null, data, (err, new_garage) ->
      return cb err if err
      console.log "ingesting new_garage: " + JSON.stringify(new_garage, null, 2)

      # create new garage object
      garage = new Garage.model(new_garage)

      garage.save (err, savedGarage) ->
        logger.error "err: " + err if err
        return cb err if err
        logger.info "created garage: " + JSON.stringify(savedGarage)
        cb null, savedGarage

  updateGarage: (id, data, cb) ->
    logger.error "garage id missing" if _.isUndefined(id) or _.isNull(id) or _.isEmpty(id)
    return cb "garage id missing" if _.isUndefined(id) or _.isNull(id) or _.isEmpty(id)
    Garage.model.findOne {_id: id}, (err, garage) ->
      return cb err if err
      return cb "garage does not exist" unless garage
      data = _.defaults data,
        name: null
        active: false
        hours: []
        event:
          cost: null
          start: null
          stop: null
        web: null
        email: null
        num_spaces: null
        phone: null
        max_height: null
        rate_card_pic_id: null
        entrance_pic_id: null
        logo_url: null
        pictures: []
        operator_id: null
        geo: []
        payment:
          cash: null
          check: null
          credit: null
        properties:
          security_cameras: null
          elevators: null
          wheelchair: null
          ev_charger: null
          wifi_zone: null
        services:
          car_wash: null
          gas_fillup: null
          security_escort: null
          flat_tire_assistance: null
          valet: null
        address: []
        rates: []
        exceptions: []
      _validate id, data, (err, update_garage) ->
        return cb err if err

        garage.name = update_garage.name
        garage.active = update_garage.active
        garage.hours = update_garage.hours
        garage.web = update_garage.web
        garage.email = update_garage.email
        garage.event = update_garage.event
        garage.address = update_garage.address
        garage.num_spaces = update_garage.num_spaces
        garage.phone = update_garage.phone
        garage.max_height = update_garage.max_height
        garage.logo_url = update_garage.logo_url
        garage.pictures = update_garage.pictures
        garage.operator_id = update_garage.operator_id
        garage.payment = update_garage.payment
        garage.properties = update_garage.properties
        garage.services = update_garage.services
        garage.rates = update_garage.rates
        garage.exceptions = update_garage.exceptions
        garage.rate_card_pic_id = update_garage.rate_card_pic_id
        garage.entrance_pic_id = update_garage.entrance_pic_id

        garage.save (err, savedGarage) ->
          logger.error "err: " + err if err
          return cb err if err
          logger.info "updated garage: " + JSON.stringify(savedGarage)
          cb null, savedGarage

  deleteGarage: (id, cb) ->
    logger.error "garage id missing" if _.isUndefined(id) or _.isNull(id) or _.isEmpty(id)
    return cb "garage id missing" if _.isUndefined(id) or _.isNull(id) or _.isEmpty(id)
    Garage.model.findOne {_id: id}, (err, garage) ->
      return cb err if err
      return cb "garage does not exist" unless garage

      # cleanup stored files in gridfs
      _.each [ garage.entrance_pic_id, garage.rate_card_pic_id], (id) ->
        gridfs_service.deleteFile id, (err, deleted) ->
          logger.info "successfully deleted file [" + id + "]"

      garage.remove (err, deletedGarage) ->
        logger.error "err: " + err if err
        return cb err if err
        logger.info "deleted garage: " + JSON.stringify(deletedGarage)
        cb null, deletedGarage

  validate: (id, data, cb) ->
    _validate id, data, cb

_validate = (id, data, cb) ->
  _validateName id, data.name, (err, validatedName) ->
    logger.error "err: " + err if err
    return cb err if err
    _validateAddress data.address, (err, validatedAddress) ->
      logger.error "err: " + err if err
      return cb err if err
      _validateNumSpaces data.num_spaces, (err, validatedNumSpaces) ->
        logger.error "err: " + err if err
        return cb err if err
        _validateActive data.active, (err, validatedActive) ->
          logger.error "err: " + err if err
          return cb err if err
          _validateEmail data.email, (err, validatedEmail) ->
            logger.error "err: " + err if err
            return cb err if err
            _validateWeb data.web, (err, validatedWeb) ->
              logger.error "err: " + err if err
              return cb err if err
              _validatePhone data.phone, (err, validatedPhone) ->
                logger.error "err: " + err if err
                return cb err if err
                _validateOperatorId data.operator_id, (err, validatedOperatorId) ->
                  logger.error "err: " + err if err
                  return cb err if err
                  _validateOperatingHours data.hours, (err, validatedOperatoringHours) ->
                    logger.error "err: " + err if err
                    return cb err if err
                    _validateHeight data.max_height, (err, validatedMaxHeight) ->
                      logger.error "err: " + err if err
                      return cb err if err
                      _validateRates data.rates, (err, validatedRates) ->
                        logger.error "err: " + err if err
                        return cb err if err
                        _validateExceptions data.exceptions, (err, validatedExceptions) ->
                          logger.error "err: " + err if err
                          return cb err if err
                          _validateLogo data.logo_url, (err, validatedLogo) ->
                            logger.error "err: " + err if err
                            return cb err if err
                            _validatePayment data.payment, (err, validatedPayment) ->
                              logger.error "err: " + err if err
                              return cb err if err
                              _validateProperties data.properties, (err, validatedProperties) ->
                                logger.error "err: " + err if err
                                return cb err if err
                                _validateServices data.services, (err, validatedServices) ->
                                  logger.error "err: " + err if err
                                  return cb err if err
                                  _validateEventBuffer data.event, (err, validatedEventBuffer) ->
                                    logger.error "err: " + err if err
                                    return cb err if err

                                    data =
                                      name: validatedName
                                      active: validatedActive
                                      hours: validatedOperatoringHours
                                      event: validatedEventBuffer
                                      web: validatedWeb
                                      email: validatedEmail
                                      num_spaces: validatedNumSpaces
                                      phone: validatedPhone
                                      max_height: validatedMaxHeight
                                      logo_url: validatedLogo
                                      operator_id: validatedOperatorId
                                      payment: validatedPayment
                                      properties: validatedProperties
                                      services: validatedServices
                                      address: validatedAddress
                                      rates: validatedRates
                                      exceptions: validatedExceptions
                                      entrance_pic_id: data.entrance_pic_id
                                      rate_card_pic_id: data.rate_card_pic_id

                                    cb null, data

_validateServices = (services, cb) ->
  logger.info "validating services ... " + JSON.stringify(services)
  cb null, services

_validateProperties = (properties, cb) ->
  logger.info "validating properties ... " + JSON.stringify(properties)
  cb null, properties

_validatePayment = (payment, cb) ->
  logger.info "validating payment ... " + JSON.stringify(payment)
  cb null, payment

_validateAddress = (address, cb) ->
  logger.info "validating address ... " + JSON.stringify(address) 
  return cb "address required" if _.isNull(address) or _.isEmpty(address)
  return cb "address street required" if _.isUndefined(address[0].street) or _.isNull(address[0].street) or _.isEmpty(validator.trim(address[0].street))
  return cb "address city required" if _.isUndefined(address[0].city) or _.isNull(address[0].city) or _.isEmpty(validator.trim(address[0].city))
  return cb "address state required" if _.isUndefined(address[0].state) or _.isNull(address[0].state) or _.isEmpty(validator.trim(validator.trim(address[0].state)))
  return cb "address zip required" if _.isUndefined(address[0].zip) or _.isNull(address[0].zip) or _.isEmpty(validator.trim(address[0].zip))
  return cb "invalid geo coordinates" if _.isUndefined(address[0].geo)
  address[0].street = validator.trim(address[0].street)
  address[0].city = validator.trim(address[0].city)
  address[0].state = validator.trim(address[0].state)
  cb null, address

_validateName = (id, name, cb) ->
  logger.info "validating garage name ... " + name
  return cb "invalid garage name" if _.isNull(name) or _.isEmpty(name) or _.isUndefined(name)
  name = validator.trim(name)
  Garage.model.findOne {name: name}, (err, garage) ->
    return cb err if err
    return cb "garage name exists" if garage and garage.id != id
    cb null, name

_validateActive = (active, cb) ->
  logger.info "verify active ... "  + JSON.stringify(active)
  return cb "active param requires boolean" if typeof active isnt "boolean"
  cb null, active

_validateWeb = (web, cb) ->
  logger.info "verify web ... " + JSON.stringify(web)
  web = validator.trim(web)
  cb null, web

_validateLogo = (logo, cb) ->
  logger.info "verify logo ... " + JSON.stringify(logo)
  return cb null, logo if _.isNull(logo)
  logo = validator.trim(logo)
  cb null, logo

_validateEmail = (email, cb) ->
  logger.info "verify email ... "  + JSON.stringify(email)
  email = validator.trim(email)
  return cb null, null if _.isNull(email) or _.isEmpty(email) or _.isUndefined(email)
  return cb "invalid email address" unless validator.isEmail(email)
  cb null, email

_validateNumSpaces = (num_spaces, cb) ->
  logger.info "verify num spaces ... "  + JSON.stringify(num_spaces)
  unless _.isNull(num_spaces) or _.isUndefined(num_spaces)
    return cb "invalid number of spaces" if _.isNaN(parseInt(num_spaces))
    return cb "number of spaces should be an integer" unless num_spaces % 1 is 0
    return cb "invalid number of spaces" if num_spaces < 1
    return cb null, num_spaces
  cb null, num_spaces

_validatePhone = (phone, cb) ->
  logger.info "verify phone ... "  + JSON.stringify(phone)
  phone = validator.trim(phone)
  cb null, phone

_validateHeight = (height, cb) ->
  MIN_HEIGHT_FEET = 5
  MAX_HEIGHT_FEET = 12
  logger.info "verify height (inches) ... " + JSON.stringify(height)
  return cb null, height if _.isNull(height)
  return cb "invalid max height" if _.isNaN(parseInt(height))
  return cb "invalid max height" if height < MIN_HEIGHT_FEET * 12 or height > MAX_HEIGHT_FEET * 12
  cb null, height

_validateType = (type, cb) ->
  logger.info "verify type ..."
  cb null, type

_validateOperatorId = (id, cb) ->
  logger.info "verify operator id ... " + id
  return cb null, null if _.isNull(id) or _.isUndefined(id) or _.isEmpty(id)
  Operator.model.findOne _id: id, (err, operator) ->
    return cb err if err
    return cb "operator does not exist" unless operator
    cb null, operator._id

_validateOperatingHours = (operating_hours, cb) ->
  logger.info "verify operating hours ... " + JSON.stringify(operating_hours)
  return cb null, operating_hours if operating_hours.length is 0
  count = 0
  _.each operating_hours, (operating_hour) ->
    return cb "'open' operating hour key required" if _.isUndefined(operating_hour.open)
    return cb "'closed' operating hour key required" if _.isUndefined(operating_hour.closed)
    return cb "'open' operating hour value required" if _.isNull(operating_hour.open) or _.isEmpty(operating_hour.open)
    return cb "'closed' operating hour value required" if _.isNull(operating_hour.closed) or _.isEmpty(operating_hour.closed)
    return cb "invalid operating hour format" if operating_hour.open.length != 5
    return cb "invalid operating hour format" if operating_hour.open.indexOf(":") != 2
    return cb "invalid operating hour format" if _.isNaN(operating_hour.open) or _.isNaN(operating_hour.closed)
    return cb "operating 'closed' hour is earlier than 'open' hour" if parseInt(operating_hour.closed) < parseInt(operating_hour.open)
    return cb "invalid operating hour duration" if operating_hour.open is operating_hour.closed
    operating_hour.open = validator.trim(operating_hour.open)
    operating_hour.closed = validator.trim(operating_hour.closed)
    count++
    cb null, operating_hours if operating_hours.length is count

# _validatePictures = (pictures, cb) ->
#   logger.info "validating pictures ... " + JSON.stringify(pictures)
#   return cb null, pictures if pictures.length is 0
#   count = 0
#   _.each pictures, (picture) ->
#     # return cb "invalid picture url" if _.isNull(picture.url) or _.isEmpty(picture.url) or _.isUndefined(picture.url)
#     # data =
#     #   path: picture.url
#     # gridfs_service.createFile data, (err, created) ->
#     #   console.log "err: " + err if err
#     #   console.log "created: " + created
#     count++
#     cb null, pictures if pictures.length is count

_validateEventBuffer = (buffer, cb) ->
  logger.info "validating event buffer ... " + JSON.stringify(buffer)
  return cb "invalid event buffer" if _.isNull(buffer) or _.isUndefined(buffer)
  return cb "event buffer 'start' key required" if _.isUndefined(buffer.start)
  return cb "event buffer 'stop' key required" if _.isUndefined(buffer.stop)
  return cb null, buffer if _.isNull(buffer.start) and _.isNull(buffer.stop)
  return cb "invalid event buffer format" if _.isNaN(parseInt(buffer.start)) or _.isNaN(parseInt(buffer.stop))
  return cb "invalid event pre buffer" if buffer.start > 3 or buffer.start < 0
  return cb "invalid event post buffer" if buffer.stop > 3 or buffer.stop < 0
  cb null, buffer

_validateRates = (rates, cb) ->
  logger.info "verify rates ... " + JSON.stringify(rates)
  return cb null, rates if rates.length is 0
  count = 0
  _.each rates, (rate) ->
    return cb "invalid rate cost" if _.isNull(rate.cost) or _.isUndefined(rate.cost)
    return cb "invalid rate cost" if rate.cost < 0
    for k,v of rate.type
      if k in ["range_hour", "range_min", "each", "flat", "early_bird", "event"]
        if v.active is true
          if once
            return cb "invalid event rate type"
          else
            once = true

    unless _.isUndefined(rate.type.range_hour)
      if rate.type.range_hour.active
        return cb "invalid rate range hour duration" if rate.type.range_hour.gt is null or rate.type.range_hour.lt is null
        return cb "invalid rate range hour start format" if rate.type.range_hour.gt.length != 5
        return cb "invalid rate range hour stop format" if rate.type.range_hour.lt.length != 5
        return cb "invalid rate range hour start format" if rate.type.range_hour.gt.indexOf(":") != 2
        return cb "invalid rate range hour stop format" if rate.type.range_hour.lt.indexOf(":") != 2
        return cb "invalid rate range hour format" if _.isNaN(rate.type.range_hour.gt) or _.isNaN(rate.type.range_hour.lt)
        return cb "invalid rate range hour duration" if parseInt(rate.type.range_hour.lt) < parseInt(rate.type.range_hour.gt)
        return cb "invalid rate range hour duration" if rate.type.range_hour.lt is rate.type.range_hour.gt

    else unless _.isUndefined(rate.type.range_min)
      if rate.type.range_min.active
        return cb "invalid rate range minute duration" if rate.type.range_min.gt is null or rate.type.range_min.lt is null
        return cb "invalid rate range minute format" if _.isNaN(parseInt(rate.type.range_min.gt)) or _.isNaN(parseInt(rate.type.range_min.lt))
        return cb "invalid rate range minute format" if parseInt(rate.type.range_min.lt) < 0 or parseInt(rate.type.range_min.gt) < 0
        return cb "invalid rate range minute duration" if parseInt(rate.type.range_min.lt) < parseInt(rate.type.range_min.gt)
        return cb "invalid rate range minute duration" if rate.type.range_min.lt is rate.type.range_min.gt

    else unless _.isUndefined(rate.type.each) 
      if rate.type.each.active
        return cb "invalid rate minute interval" if rate.type.each.minute is null
        return cb "invalid rate minute interval" if _.isNaN(parseInt(rate.type.each.minute))
        return cb "invalid rate minute interval" if rate.type.each.minute < 1 or rate.type.each.minute > 480

    else unless _.isUndefined(rate.type.flat)
      logger.warn "do something with flat rate and operational hours"
      # if rate.type.flat.active
        # verify that the operational hours are set properly

    else unless _.isUndefined(rate.type.early_bird) 
      if rate.type.early_bird.active
        return cb "invalid early bird duration" if rate.type.early_bird.start_gt is null or rate.type.early_bird.start_lt is null or rate.type.early_bird.stop_gt is null or rate.type.early_bird.stop_lt is null
        
        return cb "invalid early bird rate start start format" if rate.type.early_bird.start_gt.length != 5
        return cb "invalid early bird rate start stop format" if rate.type.early_bird.start_lt.length != 5
        return cb "invalid early bird rate stop start format" if rate.type.early_bird.stop_gt.length != 5
        return cb "invalid early bird rate stop stop format" if rate.type.early_bird.stop_lt.length != 5

        return cb "invalid early bird rate start start format" if rate.type.early_bird.start_gt.indexOf(":") != 2
        return cb "invalid early bird rate start stop format" if rate.type.early_bird.start_lt.indexOf(":") != 2
        return cb "invalid early bird rate stop start format" if rate.type.early_bird.stop_gt.indexOf(":") != 2
        return cb "invalid early bird rate stop stop format" if rate.type.early_bird.stop_lt.indexOf(":") != 2

        return cb "invalid early bird rate hour format" if _.isNaN(rate.type.early_bird.start_gt) or _.isNaN(rate.type.early_bird.start_lt)
        return cb "invalid early bird rate hour format" if _.isNaN(rate.type.early_bird.stop_gt) or _.isNaN(rate.type.early_bird.stop_lt)

        return cb "invalid early bird rate hour duration" if parseInt(rate.type.early_bird.start_lt) < parseInt(rate.type.early_bird.start_gt)
        return cb "invalid early bird rate hour duration" if parseInt(rate.type.early_bird.stop_lt) < parseInt(rate.type.early_bird.stop_gt)

        return cb "invalid early bird rate hour duration" if rate.type.early_bird.start_lt is rate.type.early_bird.start_gt or rate.type.early_bird.stop_lt is rate.type.early_bird.stop_gt

    else unless _.isUndefined(rate.type.event) 
      if rate.type.event.active
        return cb "invalid event rate name" if _.isNull(rate.type.event.name) or _.isEmpty(rate.type.event.name) or _.isUndefined(rate.type.event.name)
        return cb "invalid event rate duration" if rate.type.event.gt is null or rate.type.event.lt is null
        return cb "invalid event rate start format" if rate.type.event.gt.length != 5
        return cb "invalid event rate stop format" if rate.type.event.lt.length != 5
        return cb "invalid event rate start format" if rate.type.event.gt.indexOf(":") != 2
        return cb "invalid event rate stop format" if rate.type.event.lt.indexOf(":") != 2
        return cb "invalid event rate hour format" if _.isNaN(parseInt(rate.type.event.gt)) or _.isNaN(parseInt(rate.type.event.lt))
        return cb "invalid event rate hour duration" if parseInt(rate.type.event.lt) < parseInt(rate.type.event.gt)
        return cb "invalid event rate hour duration" if rate.type.event.lt is rate.type.event.gt
        return cb "invalid event rate description length" if rate.type.event.description.length > 100 
        return cb "invalid event rate name length" if rate.type.event.name.length > 50 

    else
      return cb "invalid event type"

    count++
    cb null, rates if rates.length is count

_validateExceptions = (exceptions, cb) ->
  logger.info "verify exceptions ... " + JSON.stringify(exceptions)
  return cb null, exceptions if exceptions.length is 0
  count = 0
  _.each exceptions, (exception) ->
    return cb "invalid exception name" if _.isNull(exception.name) or _.isEmpty(exception.name) or _.isUndefined(exception.name)
    return cb "invalid exception name length" if exception.name.length > 50
    unless _.isNull(exception.description) or _.isNull(exception.description)
      return cb "invalid exception description length" if exception.description.length > 100
      exception.description = validator.trim(exception.description)
    exception.name = validator.trim(exception.name)
    count++
    cb null, exceptions if exceptions.length is count


