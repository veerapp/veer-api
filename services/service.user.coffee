logger = require '../config/logger'
sanitize = require("validator").sanitize
Facebook = require "../models/social/facebook"
Twitter = require "../models/social/twitter"
User = require '../models/user'
_ = require 'underscore'
validator = require 'validator'

module.exports =

  getUsers: (query, cb) ->
    query = _.defaults query,
      page: 1
      per_page: 5
    query = User.model.paginate {deleted: false}, query.page, query.per_page, (err, pageCount, paginatedResults, itemCount) ->
      return cb err if err
      data =
        page_count: pageCount
        total_count: itemCount
        items: paginatedResults
      cb null, data

  getUser: (id, cb) ->
    User.model.findOne {_id: id}, (err, user) -> cb err, user

  authenticateUser: (data, cb) ->
    data = _.defaults data,
      email: null
      password: null
    logger.info "validating credentials: " + JSON.stringify(data)
    User.model.findOne {email: data.email}, (err, user) ->
      logger.error "err: " + err if err
      return cb err if err
      logger.error "err: " + "invalid credentials" unless user
      return cb "invalid credentials" unless user
      logger.error "err: " + "invalid credentials" unless _.isEqual(user.password, data.password)
      return cb "invalid credentials" unless _.isEqual(user.password, data.password)
      logger.info "validated user ... " + JSON.stringify(user)
      cb null, user

  createUser: (data, cb) ->
    data = _.defaults data,
      firstname: null
      lastname: null
      email: null
      password: null
      phone: null
    _validate data, (err, new_user) ->
      return cb err if err
      user = new User.model(new_user)
      user.twitter.push new Twitter.model()
      user.facebook.push new Facebook.model()
      user.save (err, savedUser) -> 
        logger.error "err: " + err if err
        return cb err if err
        logger.info "created user: " + JSON.stringify(savedUser)
        cb null, savedUser

  updateUser: (id, data, cb) ->
    logger.error "user id missing" if _.isUndefined(id) or _.isNull(id) or _.isEmpty(id)
    return cb "user id missing" if _.isUndefined(id) or _.isNull(id) or _.isEmpty(id)
    User.model.findOne {_id: id}, (err, user) ->
      return cb err if err
      return cb "user does not exist" unless user
      data = _.defaults data,
        firstname: null
        lastname: null
        email: null
        password: null
        phone: null
      _validate data, (err, update_user) ->
        return cb err if err
        user.email = update_user.email
        user.password = update_user.password
        user.phone = update_user.phone
        user.save (err, updatedUser) ->
          logger.error "err: " + err if err
          return cb err if err
          logger.info "updated user: " + JSON.stringify(updatedUser)
          cb null, updatedUser

  deleteUser: (id, cb) ->
    logger.error "user id missing" if _.isUndefined(id) or _.isNull(id) or _.isEmpty(id)
    return cb "user id missing" if _.isUndefined(id) or _.isNull(id) or _.isEmpty(id)
    User.model.findOne {_id: id}, (err, user) ->
      return cb err if err
      return cb "user does not exist" unless user
      user.remove (err, deletedUser) ->
        logger.error "err: " + err if err
        return cb err if err
        logger.info "deleted user: " + JSON.stringify(deletedUser)
        cb null, deletedUser

  validate: (data, cb) ->
    _validate data, cb

_validate = (new_user, cb) ->
  _validateEmail new_user.id, new_user.email, (err, validatedEmail) ->
    return cb err if err
    _validatePassword new_user.password, (err, validatedPassword) ->
      return cb err if err
      _validatePhone new_user.phone, (err, validatedPhone) ->
        return cb err if err
        _validateFirstname new_user.firstname, (err, validatedFirstname) ->
          return cb err if err
          _validatedLastname new_user.lastname, (err, validatedLastname) ->
            return cb err if err

            data =
              firstname: validatedFirstname
              lastname: validatedLastname
              email: validatedEmail
              password: validatedPassword
              phone: validatedPhone
            cb err, data

_validateFirstname = (firstname, cb) ->
  logger.info "verify firstname ... " + firstname
  cb null, firstname

_validatedLastname = (lastname, cb) ->
  logger.info "verify lastname ... " + lastname
  cb null, lastname

_validateEmail = (id, email, cb) ->
  logger.info "verify email ... " + email
  return cb "email required" if _.isNull(email) or _.isEmpty(email)
  email = validator.trim(email)
  return cb "invalid email address" unless validator.isEmail(email)
  User.model.findOne {email: email}, (err, user) ->
    return cb err if err
    return cb "email exists" if user and user.id != id
    cb null, email

_validatePassword = (password, cb) ->
  logger.info "verify password ..."
  return cb "password required" if _.isNull(password) or _.isEmpty(password)
  return cb "password needs at least 5 characters" if password.length < 5
  cb null, password

_validatePhone = (phone, cb) ->
  logger.info "verify phone ... " + phone
  phone = validator.trim(phone)
  cb null, phone
