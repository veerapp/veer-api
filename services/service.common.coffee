moment = require 'moment'
validator = require 'validator'
logger = require '../config/logger'

Venue = require '../models/garage'
Address = require '../models/address'
Operator = require '../models/operator'

_ = require 'lodash'

module.exports =

	validateEmail = (email, cb) ->