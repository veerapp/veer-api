moment = require 'moment'
validator = require 'validator'
logger = require '../config/logger'

Venue = require '../models/venue'
Address = require '../models/address'
Operator = require '../models/operator'

_ = require 'lodash'

module.exports =

  getVenues: (query, cb) ->
    query = _.defaults query,
      page: 1
      per_page: 5
      # distance: 0.7
      # latitude: 0
      # longitude: 0
      # timestamp: moment().utc().format()
      # page: 1
      # order: "desc"
    query = Venue.model.paginate {deleted: false}, query.page, query.per_page, (err, pageCount, paginatedResults, itemCount) ->
      return cb err if err
      data =
        page_count: pageCount
        total_count: itemCount
        items: paginatedResults
      cb null, data
      # "coordinates":
      #   $near: [ parseFloat(data.longitude), parseFloat(data.latitude) ]
        # $maxDistance: distance
    # query.limit data.limit
    # query.exec (err, garages) ->
    #   return cb err if err
    #   return cb null, [] if _.isEmpty garages
    #   cb null, garages

  getVenue: (id, cb) ->
    Venue.model.findOne {_id: id}, (err, venue) -> cb err, venue

  createVenue: (data, cb) ->
    data = _.defaults data,
      id: null
      name: null
      active: false
      web: null
      email: null
      phone: null
      geo: []
    _validate null, data, (err, new_venue) ->
      return cb err if err
      console.log "ingesting new_venue: " + JSON.stringify(new_venue)

      # create new venue object
      venue = new Venue.model(new_venue)

      venue.save (err, savedVenue) ->
        logger.error "err: " + err if err
        logger.info "created venue: " + JSON.stringify(savedVenue)
        cb err, savedVenue

  updateVenue: (id, data, cb) ->
    logger.error "venue id missing" if _.isUndefined(id) or _.isNull(id) or _.isEmpty(id)
    return cb "venue id missing" if _.isUndefined(id) or _.isNull(id) or _.isEmpty(id)
    Venue.model.findOne {_id: id}, (err, venue) ->
      return cb err if err
      return cb "venue does not exist" unless venue
      data = _.defaults data,
        id: null
        name: null
        active: false
        web: null
        email: null
        phone: null
        geo: []
      _validate id, data, (err, update_venue) ->
        return cb err if err

        venue.active = update_venue.active
        venue.name = update_venue.name
        venue.phone = update_venue.phone
        venue.web = update_venue.web
        venue.geo = update_venue.geo

        venue.save (err, savedVenue) ->
          logger.error "err: " + err if err
          logger.info "updated venue: " + JSON.stringify(savedVenue)
          cb null, savedVenue

  deleteVenue: (id, cb) ->
    logger.error "venue id missing" if _.isUndefined(id) or _.isNull(id) or _.isEmpty(id)
    return cb "venue id missing" if _.isUndefined(id) or _.isNull(id) or _.isEmpty(id)
    Venue.model.findOne {_id: id}, (err, venue) ->
      return cb err if err
      return cb "venue does not exist" unless venue
      venue.remove() 
      cb err, venue

  validate: (id, data, cb) ->
    _validate id, data, cb

_validate = (id, data, cb) ->
  _validateName id, data.name, (err, validatedName) ->
    logger.error "err: " + err if err
    return cb err if err
    _validateAddress data.address, (err, validatedAddress) ->
      logger.error "err: " + err if err
      return cb err if err
      _validateActive data.active, (err, validatedActive) ->
        logger.error "err: " + err if err
        return cb err if err
        _validateEmail data.email, (err, validatedEmail) ->
          logger.error "err: " + err if err
          return cb err if err
          _validateWeb data.web, (err, validatedWeb) ->
            logger.error "err: " + err if err
            return cb err if err
            _validatePhone data.phone, (err, validatedPhone) ->
              logger.error "err: " + err if err
              return cb err if err

              data =
                name: validatedName
                active: validatedActive
                web: validatedWeb
                email: validatedEmail
                phone: validatedPhone
                address: validatedAddress

              cb null, data

_validateAddress = (address, cb) ->
  logger.info "validating address ... " + JSON.stringify(address) 
  return cb "address required" if _.isNull(address) or _.isEmpty(address)
  return cb "address street required" if _.isUndefined(address[0].street) or _.isNull(address[0].street) or _.isEmpty(validator.trim(address[0].street))
  return cb "address city required" if _.isUndefined(address[0].city) or _.isNull(address[0].city) or _.isEmpty(validator.trim(address[0].city))
  return cb "address state required" if _.isUndefined(address[0].state) or _.isNull(address[0].state) or _.isEmpty(validator.trim(validator.trim(address[0].state)))
  return cb "address zip required" if _.isUndefined(address[0].zip) or _.isNull(address[0].zip) or _.isEmpty(validator.trim(address[0].zip))
  return cb "invalid geo coordinates" if _.isUndefined(address[0].geo)
  address[0].street = validator.trim(address[0].street)
  address[0].city = validator.trim(address[0].city)
  address[0].state = validator.trim(address[0].state)
  cb null, address

_validateName = (id, name, cb) ->
  logger.info "validating venue name ... " + name
  return cb "invalid venue name" if _.isNull(name) or _.isEmpty(name) or _.isUndefined(name)
  name = validator.trim(name)
  Venue.model.findOne {name: name}, (err, venue) ->
    return cb err if err
    return cb "venue name exists" if venue and venue.id != id
    cb null, name

_validateActive = (active, cb) ->
  logger.info "verify active ... "  + JSON.stringify(active)
  return cb "active param requires boolean" if typeof active isnt "boolean"
  cb null, active

_validateWeb = (web, cb) ->
  logger.info "verify web ... " + JSON.stringify(web)
  web = validator.trim(web)
  cb null, web

_validateEmail = (email, cb) ->
  logger.info "verify email ... "  + JSON.stringify(email)
  return cb null, null if _.isNull(email) or _.isEmpty(email) or _.isUndefined(email)
  email = validator.trim(email)
  return cb "invalid email address" unless validator.isEmail(email)
  cb null, email

_validatePhone = (phone, cb) ->
  logger.info "verify phone ... "  + JSON.stringify(phone)
  phone = validator.trim(phone)
  cb null, phone

_validateGeo = (geo, cb) ->
  logger.info "verify geo ... " + JSON.stringify(geo)
  return cb "geo coordinates required" if _.isNull(geo) or _.isEmpty(geo) or _.isUndefined(geo)
  return cb "invalid number of arguments" if geo.length > 2
  return cb "longitude should be a double value" if geo[0] % 1 is 0
  return cb "latitude should be a double value" if geo[1] % 1 is 0
  return cb "longitude is NaN" if _.isNaN(geo[0])
  return cb "latitude is NaN" if _.isNaN(geo[1])
  return cb "invalid longitude" if geo[0] < -180 or geo[0] > 180
  return cb "invalid latitude" if geo[1] < -90 or geo[1] > 90
  return cb "longitude requires at least 4 significant figures of precision" if geo[0].toString().split(".")[1].length < 4
  return cb "latitude requires at least 4 significant figures of precision" if geo[1].toString().split(".")[1].length < 4
  cb null, geo

