config = require '../config/config'
mongoose = require 'mongoose'
_ = require "lodash"

require('../models')()
garage_service = require '../services/service.garage'

should = require('chai').should()

describe "garage service", () -> 

  data = null

  before (done) -> 
    mongoose.connect config.DBURLTEST, () ->	
      mongoose.connection.db.dropDatabase()
      done()

  beforeEach (done) ->
    garage =
      name: _.uniqueId("garage")
      active: true
      web: "http://veer.freestyl.co"
      email: "info@veer.freestyl.co"
      num_spaces: 123
      phone: "123-123-1234"
      max_height: 96
      hours: [
        monday: true
        tuesday: true
        wednesday: true
        thursday: true
        friday:true
        open: "02:00"
        closed: "23:00"
      ,
        saturday: true
        open: "05:00"
        closed: "20:00"
      ]
      event_buffer:
        start: 1
        stop: 1
      logo_url: "http://freestyl.co/public/img/veer_logo.png"
      pictures: [
        url: "http://freestyl.co/public/img/boston1.png"
        caption: "front entrance"
        default: true
      ,
        url: "http://freestyl.co/public/img/boston2.png"
        caption: "front entrance"
        default: true
      ,
        url: "http://freestyl.co/public/img/boston3.png"
        caption: "front entrance"
        default: true
      ,
        url: "http://freestyl.co/public/img/boston4.png"
        caption: "front entrance"
        default: true
      ]
      operator_id: null
      geo: [ -71.074722, 42.349167 ]
      payment:
        cash: false
        check: false
        credit: false
      properties:
        security_cameras: false
        elevators: false
        wheelchair: false
        ev_charger: false
        wifi_zone: false
      services: 
        car_wash: false
        gas_fillup: false
        security_escort: false
        flat_tire_assistance: false
        valet: false
      address: [
        street: "12 Atlantic Ave"
        city: "Boston"
        state: "MA"
        zip: "02189"
      ]
      rates: [
        active: true
        cost: 10
        currency: "dollars"
        schedule:
          monday: true
          tuesday: true
          wednesday: true
          thursday: true
          friday: true
          saturday: true
          sunday: true
        type:
          range_hour:
            active: true
            gt: "05:00"
            lt: "08:00"
          range_min:
            active: false
            gt: null
            lt: null
          each:
            active: false
            minute: null
          flat:
            active: false
          early_bird:
            active: false
            start_gt: null
            start_lt: null
            stop_gt: null
            stop_lt: null
          event:
            active: false
            name: null
            description: null
            gt: null
            lt: null
      ]
      exceptions: [
        active: true
        name: "Thanksgiving"
        description: "enjoy thanksgiving with friends & family"
        date:
          start: "2014-11-26T14:00:00.000Z"
          stop: "2014-11-26T14:11:50.000Z"
      ,
        active: true
        name: "Christmas"
        description: "enjoy christmas with friends & family"
        date:
          start: "2014-11-26T14:00:00.000Z"
          stop: "2014-11-26T14:11:50.000Z"
      ,
        active: true
        name: "Presidents Day"
        description: null
        date:
          start: "2014-11-26T14:00:00.000Z"
          stop: "2014-11-26T14:11:50.000Z"
      ]
    data = garage
    done()

  it "creates a new garage", (done) ->
    data.name = "garage1"
    garage_service.createGarage data, (err, garage) ->
      should.not.exist err
      should.exist garage
      garage.should.be.an 'object'
      garage.name.should.equal data.name
      garage.num_spaces.should.equal data.num_spaces
      garage.phone.should.equal data.phone
      garage.max_height.should.equal data.max_height
      garage.logo_url.should.equal data.logo_url
      garage.pictures[0].url.should.equal data.pictures[0].url
      garage.pictures[0].caption.should.equal data.pictures[0].caption
      garage.pictures[0].default.should.equal data.pictures[0].default
      # garage.operator_id.should.equal data.operator_id
      garage.address[0].street.should.equal data.address[0].street
      garage.address[0].city.should.equal data.address[0].city
      garage.address[0].state.should.equal data.address[0].state
      garage.address[0].zip.should.equal data.address[0].zip
      done()

  it "does not create a garage without an address", (done) ->
    data.name = "garage7"
    data.address = undefined
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "address required"
      done()

  it "does not create a garage without a street", (done) ->
    data.name = "garage8"
    data.address[0].street = null
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "address street required"
      done()

  it "does not create a garage without a city", (done) ->
    data.name = "garage9"
    data.address[0].city = null
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "address city required"
      done()

  it "does not create a garage without a state", (done) ->
    data.name = "garage10"
    data.address[0].state = null
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "address state required"
      done()

  it "does not create a garage without a zip", (done) ->
    data.name = "garage11"
    data.address[0].zip = null
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "address zip required"
      done()

  it "does not create a garage without a street", (done) ->
    data.name = "garage12"
    data.address[0].street = " "
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "address street required"
      done()

  it "does not create a garage without a city", (done) ->
    data.name = "garage13"
    data.address[0].city = " "
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "address city required"
      done()

  it "does not create a garage without a state", (done) ->
    data.name = "garage14"
    data.address[0].state = " "
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "address state required"
      done()

  it "does not create a garage without a zip", (done) ->
    data.name = "garage15"
    data.address[0].zip =  " "
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "address zip required"
      done()

  it "does not create a garage without geo", (done) ->
    data.name = "garage17"
    data.geo = null
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "geo coordinates required"
      done()

  it "does not update a garage without an address", (done) ->
    data.name = "garage23"
    garage_service.createGarage data, (err, garage) ->
      garage.address = undefined
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "address required"
        done()

  it "does not update a garage without a street", (done) ->
    data.name = "garage24"
    garage_service.createGarage data, (err, garage) ->
      garage.address[0].street = null
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "address street required"
        done()

  it "does not update a garage without a city", (done) ->
    data.name = "garage25"
    garage_service.createGarage data, (err, garage) ->
      garage.address[0].city = null
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "address city required"
        done()

  it "does not update a garage without a state", (done) ->
    data.name = "garage26"
    garage_service.createGarage data, (err, garage) ->
      garage.address[0].state = null
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "address state required"
        done()

  it "does not update a garage without a zip", (done) ->
    data.name = "garage27"
    garage_service.createGarage data, (err, garage) ->
      garage.address[0].zip = null 
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "address zip required"
        done()

  it "does not update a garage without a street", (done) ->
    data.name = "garage28"
    garage_service.createGarage data, (err, garage) ->
      garage.address[0].street = " "
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "address street required"
        done()

  it "does not update a garage without a city", (done) ->
    data.name = "garage29"
    garage_service.createGarage data, (err, garage) ->
      garage.address[0].city = " "
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "address city required"
        done()

  it "does not update a garage without a state", (done) ->
    data.name = "garage30"
    garage_service.createGarage data, (err, garage) ->
      garage.address[0].state = " "
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "address state required"
        done()

  it "does not update a garage without a zip", (done) ->
    data.name = "garage31"
    garage_service.createGarage data, (err, garage) ->
      garage.address[0].zip = " " 
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "address zip required"
        done()

  it "does not update a garage without geo", (done) ->
    data.name = "garage33"
    garage_service.createGarage data, (err, garage) ->
      garage.geo = null
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "geo coordinates required"
        done()

  it "does not update a garage when longitude is an Integer", (done) ->
    data.name = "garage34"
    garage_service.createGarage data, (err, garage) ->
      garage.geo[0] = 75
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "longitude should be a double value"
        done()

  it "does not update a garage when latitude is an Integer", (done) ->
    data.name = "garage35"
    garage_service.createGarage data, (err, garage) ->
      garage.geo[1] = 45
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "latitude should be a double value"
        done()

  it "does not create a garage when max_height is NaN", (done) ->
    data.name = "garage36"
    data.max_height = "hello"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid max height"
      done()

  it "does not update a garage when max_height is NaN", (done) ->
    data.name = "garage37"
    garage_service.createGarage data, (err, garage) ->
      data.max_height = "hello"
      garage_service.updateGarage garage.id, data, (err, garage) ->
        should.exist err
        err.should.equal "invalid max height"
        done()

  it "does not create a garage when min_height is a 4 feet or less", (done) ->
    data.name = "garage38"
    data.max_height = 48
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid max height"
      done()

  it "does not update a garage when max_height is a greater than 12 feet", (done) ->
    data.name = "garage39"
    garage_service.createGarage data, (err, garage) ->
      garage.max_height = 156
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "invalid max height"
        done()

  it "does not create a garage w/ longitude below -180", (done) ->
    data.name = "garage40"
    data.geo = [ -181.12333, 42.349167 ]
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid longitude"
      done()

  it "does not create a garage w/ longitude above 180", (done) ->
    data.name = "garage41"
    data.geo = [ 181.12333, 42.349167 ]
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid longitude"
      done()

  it "does not create a garage w/ latitude below -90", (done) ->
    data.name = "garage42"
    data.geo = [ -75.12333, -91.12333 ]
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid latitude"
      done()

  it "does not create a garage w/ latitude above 90", (done) ->
    data.name = "garage43"
    data.geo = [ -75.12333, 91.12344 ]
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid latitude"
      done()

  it "does not update a garage w/ longitude below -180", (done) ->
    data.name = "garage44"
    garage_service.createGarage data, (err, garage) ->
      garage.geo[0] = -181.12344
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "invalid longitude"
        done()

  it "does not update a garage w/ longitude above 180", (done) ->
    data.name = "garage45"
    garage_service.createGarage data, (err, garage) ->
      garage.geo[0] = 181.12344
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "invalid longitude"
        done()

  it "does not update a garage w/ latitude below -90", (done) ->
    data.name = "garage46"
    garage_service.createGarage data, (err, garage) ->
      garage.geo[1] = -91.12344
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "invalid latitude"
        done()

  it "does not update a garage w/ latitude above 90", (done) ->
    data.name = "garage47"
    garage_service.createGarage data, (err, garage) ->
      garage.geo[1] = 91.12344
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "invalid latitude"
        done()

  it "does not create a garage w/ number of spaces less than 1", (done) ->
    data.name = "garage49"
    data.num_spaces = 0
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid number of spaces"
      done()

  it "does not update a garage w/ number of spaces less than 1", (done) ->
    data.name = "garage51"
    garage_service.createGarage data, (err, garage) ->
      garage.num_spaces = 0
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "invalid number of spaces"
        done()

  it "does not create a garage w/ number of spaces that is a double", (done) ->
    data.name = "garage52"
    data.num_spaces = 1.1
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "number of spaces should be an integer"
      done()

  it "does not update a garage w/ number of spaces that is a double", (done) ->
    data.name = "garage53"
    garage_service.createGarage data, (err, garage) ->
      garage.num_spaces = 1.1
      garage_service.updateGarage garage.id, garage, (err, garage) ->
        should.exist err
        err.should.equal "number of spaces should be an integer"
        done()

  it "should create garage w/ all properties enabled", (done) ->
    data.name = "garage54"
    data.properties.security_cameras = true
    data.properties.elevators = true
    data.properties.wheelchair = true
    data.properties.ev_charger = true
    data.properties.wifi_zone = true
    garage_service.createGarage data, (err, garage) ->
      should.not.exist err
      garage.properties.security_cameras.should.equal data.properties.security_cameras
      garage.properties.elevators.should.equal data.properties.elevators
      garage.properties.wheelchair.should.equal data.properties.wheelchair
      garage.properties.ev_charger.should.equal data.properties.ev_charger
      garage.properties.wifi_zone.should.equal data.properties.wifi_zone
      done()

  it "should create a garage w/ all services enabled", (done) ->
    data.name = "garage55"
    data.services.car_wash = true
    data.services.gas_fillup = true
    data.services.security_escort = true
    data.services.flat_tire_assistance = true
    data.services.valet = true
    garage_service.createGarage data, (err, garage) ->
      should.not.exist err
      garage.services.car_wash.should.equal data.services.car_wash
      garage.services.gas_fillup.should.equal data.services.gas_fillup
      garage.services.security_escort.should.equal data.services.security_escort
      garage.services.flat_tire_assistance.should.equal data.services.flat_tire_assistance
      garage.services.valet.should.equal data.services.valet
      done()

  it "should create a garage w/ all payments enabled", (done) ->
    data.name = "garage56"
    data.payment.cash = true
    data.payment.credit = true
    data.payment.check = true
    garage_service.createGarage data, (err, garage) ->
      should.not.exist err
      garage.payment.cash.should.equal data.payment.cash
      garage.payment.credit.should.equal data.payment.credit
      garage.payment.check.should.equal data.payment.check
      done()

  it "should update garage w/ all properties enabled", (done) ->
    data.name = "garage57"
    garage_service.createGarage data, (err, garage) ->
      garage.properties.security_cameras = true
      garage.properties.elevators = true
      garage.properties.wheelchair = true
      garage.properties.ev_charger = true
      garage.properties.wifi_zone = true
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.not.exist err
        garage.properties.security_cameras.should.equal updatedGarage.properties.security_cameras
        garage.properties.elevators.should.equal updatedGarage.properties.elevators
        garage.properties.wheelchair.should.equal updatedGarage.properties.wheelchair
        garage.properties.ev_charger.should.equal updatedGarage.properties.ev_charger
        garage.properties.wifi_zone.should.equal updatedGarage.properties.wifi_zone
        done()

  it "should update a garage w/ all services enabled", (done) ->
    data.name = "garage58"
    garage_service.createGarage data, (err, garage) ->
      garage.services.car_wash = true
      garage.services.gas_fillup = true
      garage.services.security_escort = true
      garage.services.flat_tire_assistance = true
      garage.services.valet = true
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.not.exist err
        garage.services.car_wash.should.equal updatedGarage.services.car_wash
        garage.services.gas_fillup.should.equal updatedGarage.services.gas_fillup
        garage.services.security_escort.should.equal updatedGarage.services.security_escort
        garage.services.flat_tire_assistance.should.equal updatedGarage.services.flat_tire_assistance
        garage.services.valet.should.equal updatedGarage.services.valet
        done()

  it "should update a garage w/ all payments enabled", (done) ->
    data.name = "garage59"
    garage_service.createGarage data, (err, garage) ->
      garage.payment.cash = true
      garage.payment.credit = true
      garage.payment.check = true
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.not.exist err
        garage.payment.cash.should.equal updatedGarage.payment.cash
        garage.payment.credit.should.equal updatedGarage.payment.credit
        garage.payment.check.should.equal updatedGarage.payment.check
        done()

  it "should update a garage to inactive", (done) ->
    data.name = "garage60"
    garage_service.createGarage data, (err, garage) ->
      garage.active = false
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.not.exist err
        garage.active.should.equal updatedGarage.active
        done()

  it "should update a garage name", (done) ->
    data.name = "garage61"
    garage_service.createGarage data, (err, garage) ->
      garage.name = _.uniqueId("garage_update_")
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.not.exist err
        garage.name.should.equal updatedGarage.name
        done()

  it "should not create a garage with null name", (done) ->
    data.name = null
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid garage name"
      done()

  it "should not update a garage with null name", (done) ->
    data.name = "garage62"
    garage_service.createGarage data, (err, garage) ->
      garage.name = null
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid garage name"
        done()

  it "should not create garage when active is non-boolean", (done) ->
    data.name = "garage63"
    data.active = null
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "active param requires boolean"
      done()

  it "should not update garage when active is non-boolean", (done) ->
    data.name = "garage64"
    garage_service.createGarage data, (err, garage) ->
      garage.active = null
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "active param requires boolean"
        done()

  it "should not create a garage with a longitude w/ less than 4 significant figures of precision", (done) ->
    data.name = "garage67"
    data.geo[0] = 75.123
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "longitude requires at least 4 significant figures of precision"
      done()

  it "should not create a garage with a latitude w/ less than 4 significant figures of precision", (done) ->
    data.name = "garage68"
    data.geo[1] = -45.123
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "latitude requires at least 4 significant figures of precision"
      done()

  it "should not update a garage with a longitude w/ less than 4 significant figures of precision", (done) ->
    data.name = "garage69"
    garage_service.createGarage data, (err, garage) ->
      garage.geo[0] = 75.123
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "longitude requires at least 4 significant figures of precision"
        done()

  it "should not update a garage with a latitude w/ less than 4 significant figures of precision", (done) ->
    data.name = "garage70"
    garage_service.createGarage data, (err, garage) ->
      garage.geo[1] = 75.123
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "latitude requires at least 4 significant figures of precision"
        done()

  it "should trim garage name on create", (done) ->
    NAME = "garage71"
    data.name = " " + NAME + " "
    garage_service.createGarage data, (err, garage) ->
      garage.name.should.equal NAME
      done()

  it "should trim garage web url on create", (done) ->
    data.name = "garage72"
    WEB = data.web
    data.web = " " + WEB + " "
    garage_service.createGarage data, (err, garage) ->
      garage.web.should.equal WEB
      done()

  it "should trim garage email on create", (done) ->
    data.name = "garage73"
    EMAIL = data.email
    data.email = " " + EMAIL + " "
    garage_service.createGarage data, (err, garage) ->
      garage.email.should.equal EMAIL
      done()

  it "should trim garage phone on create", (done) ->
    data.name = "garage74"
    PHONE = data.phone
    data.phone = " " + PHONE + " "
    garage_service.createGarage data, (err, garage) ->
      garage.phone.should.equal PHONE
      done()

  it "should trim garage street on create", (done) ->
    data.name = "garage75"
    STREET = data.address[0].street
    data.address[0].street = " " + STREET + " "
    garage_service.createGarage data, (err, garage) ->
      garage.address[0].street.should.equal STREET
      done()

  it "should trim garage city on create", (done) ->
    data.name = "garage76"
    CITY = data.address[0].city
    data.address[0].city = " " + CITY + " "
    garage_service.createGarage data, (err, garage) ->
      garage.address[0].city.should.equal CITY
      done()

  it "should trim garage state on create", (done) ->
    data.name = "garage77"
    STATE = data.address[0].state
    data.address[0].state = " " + STATE + " "
    garage_service.createGarage data, (err, garage) ->
      garage.address[0].state.should.equal STATE
      done()

  it "should trim garage logo url on create", (done) ->
    data.name = "garage78"
    LOGO = data.logo_url
    data.logo_url = " " + LOGO + " "
    garage_service.createGarage data, (err, garage) ->
      garage.logo_url.should.equal LOGO
      done()

  it "should trim garage picture url on create", (done) ->
    data.name = "garage79"
    URL = data.pictures[0].url
    data.pictures[0].url = " " + URL + " "
    garage_service.createGarage data, (err, garage) ->
      garage.pictures[0].url.should.equal URL
      done()

  it "should trim garage picture caption on create", (done) ->
    data.name = "garage80"
    CAPTION = data.pictures[0].caption
    data.pictures[0].caption = " " + CAPTION + " "
    garage_service.createGarage data, (err, garage) ->
      garage.pictures[0].caption.should.equal CAPTION
      done()

  it "should not create garage picture with null url", (done) ->
    data.name = "garage81"
    data.pictures[0].url = null
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid picture url"
      done()

  it "should trim garage name on update", (done) ->
    data.name = "garage82"
    garage_service.createGarage data, (err, garage) ->
      NAME = garage.name
      garage.name = " " + NAME + " "
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        updatedGarage.name.should.equal NAME
        done()

  it "should trim garage web url on update", (done) ->
    data.name = "garage83"
    garage_service.createGarage data, (err, garage) ->
      WEB = garage.web
      garage.web = " " + WEB + " "
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        updatedGarage.web.should.equal WEB
        done()

  it "should trim garage email on update", (done) ->
    data.name = "garage84"
    garage_service.createGarage data, (err, garage) ->
      EMAIL = garage.email
      garage.email = " " + EMAIL + " "
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        updatedGarage.email.should.equal EMAIL
        done()

  it "should trim garage phone on update", (done) ->
    data.name = "garage85"
    garage_service.createGarage data, (err, garage) ->
      PHONE = garage.phone
      garage.phone = " " + PHONE + " "
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        updatedGarage.phone.should.equal PHONE
        done()

  it "should trim garage street on update", (done) ->
    data.name = "garage86"
    garage_service.createGarage data, (err, garage) ->
      STREET = data.address[0].street
      garage.address[0].street = " " + STREET + " "
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        updatedGarage.address[0].street.should.equal STREET
        done()

  it "should trim garage city on update", (done) ->
    data.name = "garage87"
    garage_service.createGarage data, (err, garage) ->
      CITY = data.address[0].city
      garage.address[0].city = " " + CITY + " "
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        updatedGarage.address[0].city.should.equal CITY
        done()

  it "should trim garage state on update", (done) ->
    data.name = "garage88"
    garage_service.createGarage data, (err, garage) ->
      STATE = data.address[0].state
      garage.address[0].state = " " + STATE + " "
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        updatedGarage.address[0].state.should.equal STATE
        done()

  it "should trim garage logo url on update", (done) ->
    data.name = "garage89"
    garage_service.createGarage data, (err, garage) ->
      LOGO = data.logo_url
      garage.logo_url = " " + LOGO + " "
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        updatedGarage.logo_url.should.equal LOGO
        done()

  it "should trim garage picture url on update", (done) ->
    data.name = "garage90"
    garage_service.createGarage data, (err, garage) ->
      URL = data.pictures[0].url
      garage.pictures[0].url = " " + URL + " "
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        updatedGarage.pictures[0].url.should.equal URL
        done()

  it "should trim garage picture caption on update", (done) ->
    data.name = "garage91"
    garage_service.createGarage data, (err, garage) ->
      CAPTION = data.pictures[0].caption
      garage.pictures[0].caption = " " + CAPTION + " "
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        updatedGarage.pictures[0].caption.should.equal CAPTION
        done()

  it "should trim exception name on create", (done) ->
    data.name = "garage92"
    NAME = data.exceptions[0].name
    data.exceptions[0].name = " " + NAME + " "
    garage_service.createGarage data, (err, garage) ->
      garage.exceptions[0].name.should.equal NAME
      done()

  it "should trim exception description on create", (done) ->
    data.name = "garage93"
    DESCRIPTION = data.exceptions[0].description
    data.exceptions[0].description = " " + DESCRIPTION + " "
    garage_service.createGarage data, (err, garage) ->
      garage.exceptions[0].description.should.equal DESCRIPTION
      done()

  it "should not create exception with null name on create", (done) ->
    data.name = "garage94"
    data.exceptions[0].name = null
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid exception name"
      done()

  it "should not allow exception stop time to be earlier than start time on create", (done) ->
    done("not implemented")

  it "should not allow exception stop time to be earlier than start time on update", (done) ->
    done("not implemented")

  it "should trim exception name on update", (done) ->
    data.name = "garage95"
    garage_service.createGarage data, (err, garage) ->
      NAME = data.exceptions[0].name
      garage.exceptions[0].name = " " + NAME + " "
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        updatedGarage.exceptions[0].name.should.equal NAME
        done()

  it "should trim exception description on update", (done) ->
    data.name = "garage96"
    garage_service.createGarage data, (err, garage) ->
      DESCRIPTION = data.exceptions[0].description
      garage.exceptions[0].description = " " + DESCRIPTION + " "
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        updatedGarage.exceptions[0].description.should.equal DESCRIPTION
        done()

  it "should not allow a rate with no 'active' type", (done) ->
    data.name = "garage97"
    data.rates[0].type.range_hour.active = false
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid event type"
      done()

  it "should not allow a rate when more than one type is 'active'", (done) ->
    data.name = "garage98"
    data.rates[0].type.range_hour.active = true
    data.rates[0].type.event.active = true
    data.rates[0].type.flat.active = true
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid event rate type"
      done()

  it "should not allow rate stop hour to be before rate start hour for 'Hour Range' type on create", (done) ->
    data.name = "garage99"
    data.rates[0].type.range_hour.gt = "10:00"
    data.rates[0].type.range_hour.lt = "08:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid rate range hour duration"
      done()

  it "should not allow rate stop hour to be the same as rate start hour for 'Hour Range' type on create", (done) ->
    data.name = "garage100"
    data.rates[0].type.range_hour.gt = "10:00"
    data.rates[0].type.range_hour.lt = "10:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid rate range hour duration"
      done()

  it "should not allow rate stop hour to be null on create", (done) ->
    data.name = "garage101"
    data.rates[0].type.range_hour.gt = "10:00"
    data.rates[0].type.range_hour.lt = null
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid rate range hour duration"
      done()

  it "should not allow rate start hour to be null on create", (done) ->
    data.name = "garage102"
    data.rates[0].type.range_hour.gt = null
    data.rates[0].type.range_hour.lt = "10:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid rate range hour duration"
      done()

  it "should not allow rate stop minute to equal rate start minute on create", (done) ->
    data.name = "garage103"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.range_min.active = true
    data.rates[0].type.range_min.gt = "10:00"
    data.rates[0].type.range_min.lt = "10:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid rate range minute duration"
      done()

  it "should not allow rate stop minute to be null on create", (done) ->
    data.name = "garage104"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.range_min.active = true
    data.rates[0].type.range_min.gt = "10:00"
    data.rates[0].type.range_min.lt = null
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid rate range minute duration"
      done()

  it "should not allow rate start minute to be null on create", (done) ->
    data.name = "garage105"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.range_min.active = true
    data.rates[0].type.range_min.gt = null
    data.rates[0].type.range_min.lt = "10:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid rate range minute duration"
      done()

  it "should not allow rate each to have null minute on create", (done) ->
    data.name = "garage106"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.each.active = true
    data.rates[0].type.each.minute = null
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid rate minute interval"
      done()

  it "should not allow rate each to have a NaN minute on create", (done) ->
    data.name = "garage107"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.each.active = true
    data.rates[0].type.each.minute = "hello"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid rate minute interval"
      done()

  it "should not allow rate each to have a minute less than 1 on create", (done) ->
    data.name = "garage108"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.each.active = true
    data.rates[0].type.each.minute = 0
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid rate minute interval"
      done()

  it "should not allow rate each to have a minute greater than 480 minutes on create", (done) ->
    data.name = "garage109"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.each.active = true
    data.rates[0].type.each.minute = 481
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid rate minute interval"
      done()

  it "should not allow early bird rate start time to equal stop time on create", (done) ->
    data.name = "garage110"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.early_bird.active = true
    data.rates[0].type.early_bird.start_gt = "07:00"
    data.rates[0].type.early_bird.start_lt = "07:00"
    data.rates[0].type.early_bird.stop_gt = "19:00"
    data.rates[0].type.early_bird.stop_lt = "20:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid early bird rate hour duration"
      done()

  it "should not allow early bird rate to have null start time start on create", (done) ->
    data.name = "garage111"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.early_bird.active = true
    data.rates[0].type.early_bird.start_gt = null
    data.rates[0].type.early_bird.start_lt = "07:00"
    data.rates[0].type.early_bird.stop_gt = "19:00"
    data.rates[0].type.early_bird.stop_lt = "20:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid early bird duration"
      done()

  it "should not allow early bird rate to have null start time stop on create", (done) ->
    data.name = "garage112"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.early_bird.active = true
    data.rates[0].type.early_bird.start_gt = "05:00"
    data.rates[0].type.early_bird.start_lt = null
    data.rates[0].type.early_bird.stop_gt = "19:00"
    data.rates[0].type.early_bird.stop_lt = "20:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid early bird duration"
      done()

  it "should not allow early bird rate to have null stop time start on create", (done) ->
    data.name = "garage113"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.early_bird.active = true
    data.rates[0].type.early_bird.start_gt = "05:00"
    data.rates[0].type.early_bird.start_lt = "07:00"
    data.rates[0].type.early_bird.stop_gt = null
    data.rates[0].type.early_bird.stop_lt = "20:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid early bird duration"
      done()

  it "should not allow early bird rate start time to equal stop time start on create", (done) ->
    data.name = "garage114"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.early_bird.active = true
    data.rates[0].type.early_bird.start_gt = "05:00"
    data.rates[0].type.early_bird.start_lt = "07:00"
    data.rates[0].type.early_bird.stop_gt = "20:00"
    data.rates[0].type.early_bird.stop_lt = "20:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid early bird rate hour duration"
      done()

  it "should not allow early bird rate to have null stop time stop on create", (done) ->
    data.name = "garage115"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.early_bird.active = true
    data.rates[0].type.early_bird.start_gt = "05:00"
    data.rates[0].type.early_bird.start_lt = "07:00"
    data.rates[0].type.early_bird.stop_gt = "19:00"
    data.rates[0].type.early_bird.stop_lt = null
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid early bird duration"
      done()

  it "should not allow early bird rate start time stop hour to be earlier than start time start hour on create", (done) ->
    data.name = "garage116"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.early_bird.active = true
    data.rates[0].type.early_bird.start_gt = "07:00"
    data.rates[0].type.early_bird.start_lt = "05:00"
    data.rates[0].type.early_bird.stop_gt = "19:00"
    data.rates[0].type.early_bird.stop_lt = "20:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid early bird rate hour duration"
      done()

  it "should not allow early bird rate start time stop hour to be the same than start time start hour on create", (done) ->
    data.name = "garage117"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.early_bird.active = true
    data.rates[0].type.early_bird.start_gt = "05:00"
    data.rates[0].type.early_bird.start_lt = "05:00"
    data.rates[0].type.early_bird.stop_gt = "19:00"
    data.rates[0].type.early_bird.stop_lt = "20:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid early bird rate hour duration"
      done()

  it "should not allow early bird rate stop time stop hour to be earlier than stop time start hour on create", (done) ->
    data.name = "garage118"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.early_bird.active = true
    data.rates[0].type.early_bird.start_gt = "05:00"
    data.rates[0].type.early_bird.start_lt = "07:00"
    data.rates[0].type.early_bird.stop_gt = "20:00"
    data.rates[0].type.early_bird.stop_lt = "19:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid early bird rate hour duration"
      done()

  it "should not allow early bird rate stop time stop hour to be the same as stop time start hour on create", (done) ->
    data.name = "garage119"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.early_bird.active = true
    data.rates[0].type.early_bird.start_gt = "05:00"
    data.rates[0].type.early_bird.start_lt = "07:00"
    data.rates[0].type.early_bird.stop_gt = "20:00"
    data.rates[0].type.early_bird.stop_lt = "20:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid early bird rate hour duration"
      done()

  it "should not allow event rate stop time hour to be earlier than start time hour on create", (done) ->
    data.name = "garage120"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.event.active = true
    data.rates[0].type.event.name = "event #1"
    data.rates[0].type.event.gt = "07:00"
    data.rates[0].type.event.lt = "05:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid event rate hour duration"
      done()

  it "should not allow event rate stop time hour and start time hour to be the same on create", (done) ->
    data.name = "garage121"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.event.active = true
    data.rates[0].type.event.name = "event #1"
    data.rates[0].type.event.gt = "07:00"
    data.rates[0].type.event.lt = "07:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid event rate hour duration"
      done()

  it "does not create an event rate with a null name on create", (done) ->
    data.name = "garage122"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.event.active = true
    data.rates[0].type.event.name = null
    data.rates[0].type.event.gt = "07:00"
    data.rates[0].type.event.lt = "09:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid event rate name"
      done()

  it "does not create an event rate description greater than 100 characters on create", (done) ->
    data.name = "garage123"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.event.active = true
    data.rates[0].type.event.name = "patriots event"
    data.rates[0].type.event.description = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
    data.rates[0].type.event.gt = "07:00"
    data.rates[0].type.event.lt = "09:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid event rate description length"
      done()

  it "does not create an event rate name greater than 50 characters on create", (done) ->
    data.name = "garage124"
    data.rates[0].type.range_hour.active = false
    data.rates[0].type.event.active = true
    data.rates[0].type.event.name = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
    data.rates[0].type.event.description = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
    data.rates[0].type.event.gt = "07:00"
    data.rates[0].type.event.lt = "09:00"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid event rate name length"
      done()

  # update

  it "should not allow rate stop hour to be before rate start hour for 'Hour Range' type on update", (done) ->
    data.name = "garage125"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.gt = "10:00"
      garage.rates[0].type.range_hour.lt = "08:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid rate range hour duration"
        done()

  it "should not allow rate stop hour to be the same as rate start hour for 'Hour Range' type on update", (done) ->
    data.name = "garage126"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.gt = "10:00"
      garage.rates[0].type.range_hour.lt = "10:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid rate range hour duration"
        done()


  it "should not allow rate stop hour to be null on update", (done) ->
    data.name = "garage127"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.gt = "10:00"
      garage.rates[0].type.range_hour.lt = null
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid rate range hour duration"
        done()

  it "should not allow rate start hour to be null on update", (done) ->
    data.name = "garage128"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.gt = null
      garage.rates[0].type.range_hour.lt = "10:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid rate range hour duration"
        done()


  it "should not allow rate stop minute to equal rate start minute on update", (done) ->
    data.name = "garage129"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.range_min.active = true
      garage.rates[0].type.range_min.gt = "10:00"
      garage.rates[0].type.range_min.lt = "10:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid rate range minute duration"
        done()

  it "should not allow rate stop minute to be null on update", (done) ->
    data.name = "garage130"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.range_min.active = true
      garage.rates[0].type.range_min.gt = "10:00"
      garage.rates[0].type.range_min.lt = null
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid rate range minute duration"
        done()

  it "should not allow rate start minute to be null on update", (done) ->
    data.name = "garage131"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.range_min.active = true
      garage.rates[0].type.range_min.gt = null
      garage.rates[0].type.range_min.lt = "10:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid rate range minute duration"
        done()

  it "should not allow rate each to have null minute on update", (done) ->
    data.name = "garage132"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.each.active = true
      garage.rates[0].type.each.minute = null
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid rate minute interval"
        done()

  it "should not allow rate each to have a NaN minute on update", (done) ->
    data.name = "garage133"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.each.active = true
      garage.rates[0].type.each.minute = "hello"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid rate minute interval"
        done()

  it "should not allow rate each to have a minute less than 1 on update", (done) ->
    data.name = "garage134"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.each.active = true
      garage.rates[0].type.each.minute = 0
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid rate minute interval"
        done()

  it "should not allow rate each to have a minute greater than 480 minutes on update", (done) ->
    data.name = "garage135"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.each.active = true
      garage.rates[0].type.each.minute = 481
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid rate minute interval"
        done()

  it "should not allow early bird rate start time to equal stop time start on update", (done) ->
    data.name = "garage136"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.early_bird.active = true
      garage.rates[0].type.early_bird.start_gt = "07:00"
      garage.rates[0].type.early_bird.start_lt = "07:00"
      garage.rates[0].type.early_bird.stop_gt = "19:00"
      garage.rates[0].type.early_bird.stop_lt = "20:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid early bird rate hour duration"
        done()

  it "should not allow early bird rate to have null start time start on update", (done) ->
    data.name = "garage137"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.early_bird.active = true
      garage.rates[0].type.early_bird.start_gt = null
      garage.rates[0].type.early_bird.start_lt = "07:00"
      garage.rates[0].type.early_bird.stop_gt = "19:00"
      garage.rates[0].type.early_bird.stop_lt = "20:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid early bird duration"
        done()

  it "should not allow early bird rate to have null start time stop on update", (done) ->
    data.name = "garage138"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.early_bird.active = true
      garage.rates[0].type.early_bird.start_gt = "05:00"
      garage.rates[0].type.early_bird.start_lt = null
      garage.rates[0].type.early_bird.stop_gt = "19:00"
      garage.rates[0].type.early_bird.stop_lt = "20:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid early bird duration"
        done()

  it "should not allow early bird rate to have null stop time start on update", (done) ->
    data.name = "garage139"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.early_bird.active = true
      garage.rates[0].type.early_bird.start_gt = "05:00"
      garage.rates[0].type.early_bird.start_lt = "07:00"
      garage.rates[0].type.early_bird.stop_gt = null
      garage.rates[0].type.early_bird.stop_lt = "20:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid early bird duration"
        done()

  it "should not allow early bird rate to have equal stop time start on update", (done) ->
    data.name = "garage140"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.early_bird.active = true
      garage.rates[0].type.early_bird.start_gt = "05:00"
      garage.rates[0].type.early_bird.start_lt = "07:00"
      garage.rates[0].type.early_bird.stop_gt = "20:00"
      garage.rates[0].type.early_bird.stop_lt = "20:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid early bird rate hour duration"
        done()

  it "should not allow early bird rate to have null stop time stop on update", (done) ->
    data.name = "garage141"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.early_bird.active = true
      garage.rates[0].type.early_bird.start_gt = "05:00"
      garage.rates[0].type.early_bird.start_lt = "07:00"
      garage.rates[0].type.early_bird.stop_gt = "19:00"
      garage.rates[0].type.early_bird.stop_lt = null
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid early bird duration"
        done()

  it "should not allow early bird rate start time stop hour to be earlier than start time start hour on update", (done) ->
    data.name = "garage142"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.early_bird.active = true
      garage.rates[0].type.early_bird.start_gt = "07:00"
      garage.rates[0].type.early_bird.start_lt = "05:00"
      garage.rates[0].type.early_bird.stop_gt = "19:00"
      garage.rates[0].type.early_bird.stop_lt = "20:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid early bird rate hour duration"
        done()

  it "should not allow early bird rate start time stop hour to be the same as start time start hour on update", (done) ->
    data.name = "garage143"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.early_bird.active = true
      garage.rates[0].type.early_bird.start_gt = "07:00"
      garage.rates[0].type.early_bird.start_lt = "07:00"
      garage.rates[0].type.early_bird.stop_gt = "19:00"
      garage.rates[0].type.early_bird.stop_lt = "20:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid early bird rate hour duration"
        done()

  it "should not allow early bird rate stop time stop hour to be earlier than stop time start hour on update", (done) ->
    data.name = "garage144"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.early_bird.active = true
      garage.rates[0].type.early_bird.start_gt = "05:00"
      garage.rates[0].type.early_bird.start_lt = "07:00"
      garage.rates[0].type.early_bird.stop_gt = "20:00"
      garage.rates[0].type.early_bird.stop_lt = "19:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid early bird rate hour duration"
        done()

  it "should not allow early bird rate stop time stop hour to be the same as the stop time start hour on update", (done) ->
    data.name = "garage145"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.early_bird.active = true
      garage.rates[0].type.early_bird.start_gt = "05:00"
      garage.rates[0].type.early_bird.start_lt = "07:00"
      garage.rates[0].type.early_bird.stop_gt = "20:00"
      garage.rates[0].type.early_bird.stop_lt = "20:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid early bird rate hour duration"
        done()

  it "should not allow event rate stop time hour and start time hour to be the same on update", (done) ->
    data.name = "garage146"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.event.name = "celtics event parking"
      garage.rates[0].type.event.active = true
      garage.rates[0].type.event.gt = "07:00"
      garage.rates[0].type.event.lt = "07:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid event rate hour duration"
        done()

  it "does not create an event rate with a null name on update", (done) ->
    data.name = "garage147"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.event.name = null
      garage.rates[0].type.event.active = true
      garage.rates[0].type.event.gt = "07:00"
      garage.rates[0].type.event.lt = "09:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid event rate name"
        done()

  it "does not create an event rate with a undefined name on update", (done) ->
    data.name = "garage148"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.event.name = undefined
      garage.rates[0].type.event.active = true
      garage.rates[0].type.event.gt = "07:00"
      garage.rates[0].type.event.lt = "09:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid event rate name"
        done()

  it "does not create an event rate description greater than 100 characters on update", (done) ->
    data.name = "garage149"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.event.name = "bruins hockey event"
      garage.rates[0].type.event.description = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
      garage.rates[0].type.event.active = true
      garage.rates[0].type.event.gt = "07:00"
      garage.rates[0].type.event.lt = "09:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid event rate description length"
        done()

  it "does not create an event rate name greater than 50 characters on update", (done) ->
    data.name = "garage150"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].type.range_hour.active = false
      garage.rates[0].type.event.name = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
      garage.rates[0].type.event.description = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
      garage.rates[0].type.event.active = true
      garage.rates[0].type.event.gt = "07:00"
      garage.rates[0].type.event.lt = "09:00"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid event rate name length"
        done()

  it "does not create a rate with null cost on create", (done) ->
    data.name = "garage151"
    data.rates[0].cost = null
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid rate cost"
      done()

  it "does not create a rate with null cost on update", (done) ->
    data.name = "garage152"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].cost = null
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid rate cost"
        done()

  it "does not create a rate with undefined cost on create", (done) ->
    data.name = "garage153"
    data.rates[0].cost = undefined
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid rate cost"
      done()

  it "does not create a rate with undefined cost on update", (done) ->
    data.name = "garage154"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].cost = undefined
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid rate cost"
        done()

  it "does not create a rate with negative cost on create", (done) ->
    data.name = "garage155"
    data.rates[0].cost = -1
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid rate cost"
      done()

  it "does not create a rate with negative cost on update", (done) ->
    data.name = "garage156"
    garage_service.createGarage data, (err, garage) ->
      garage.rates[0].cost = -1
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid rate cost"
        done()

  it "does not update garage with null id", (done) ->
    data.name = "garage157"
    garage_service.createGarage data, (err, garage) ->
      garage_service.updateGarage null, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "garage id missing"
        done()

  it "does not update garage with undefined id", (done) ->
    data.name = "garage158"
    garage_service.createGarage data, (err, garage) ->
      garage_service.updateGarage undefined, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "garage id missing"
        done()

  it "does not update garage with empty id", (done) ->
    data.name = "garage159"
    garage_service.createGarage data, (err, garage) ->
      garage_service.updateGarage "", garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "garage id missing"
        done()

  it "does not create garage with invalid email", (done) ->
    data.name = "garage160"
    data.email = "admin@ddddddddddddd"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid email address"
      done()

  it "does not update garage with invalid email", (done) ->
    data.name = "garage161"
    garage_service.createGarage data, (err, garage) ->
      garage.email = "admin@ddddddd"
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid email address"
        done()

  it "creates garage with null email", (done) ->
    data.name = "garage162"
    data.email = null
    garage_service.createGarage data, (err, garage) ->
      should.not.exist err
      return done("err: garage email isnt not null") if garage.email isnt null
      done()

  it "update garage with null email", (done) ->
    data.name = "garage163"
    garage_service.createGarage data, (err, garage) ->
      garage.email = null
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.not.exist err
        return done("err: garage email isnt not null") if garage.email isnt null
        done()

  it "creates garage with undefined email", (done) ->
    data.name = "garage164"
    data.email = undefined
    garage_service.createGarage data, (err, garage) ->
      should.not.exist err
      return done("err: garage email isnt not null") if garage.email isnt null
      done()

  it "updates garage with invalid email", (done) ->
    data.name = "garage165"
    garage_service.createGarage data, (err, garage) ->
      garage.email = undefined
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.not.exist err
        return done("err: garage email isnt not null") if garage.email isnt null
        done()

  it "creates garage with empty email on create", (done) ->
    data.name = "garage166"
    data.email = ""
    garage_service.createGarage data, (err, garage) ->
      should.not.exist err
      return done("err: garage email isnt not null") if garage.email isnt null
      done()

  it "updates garage with empty email on update", (done) ->
    garage_service.createGarage data, (err, garage) ->
      garage.email = ""
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.not.exist err
        return done("err: garage email isnt not null") if updatedGarage.email isnt null
        done()

  it "should not allow event pre buffer to exceed 3 hours on create", (done) ->
    data.name = "garage167"
    data.event_buffer.start = 4
    data.event_buffer.stop = 3
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid event pre buffer"
      done()

  it "should not allow event post buffer to exceed 3 hours on create", (done) ->
    data.name = "garage168"
    data.event_buffer.start = 3
    data.event_buffer.stop = 4
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid event post buffer"
      done()

  it "should not allow event pre buffer to exceed 3 hours on update", (done) ->
    data.name = "garage169"
    garage_service.createGarage data, (err, garage) ->
      garage.event_buffer.start = 4
      garage.event_buffer.stop = 3
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid event pre buffer"
        done()

  it "should not allow event post buffer to exceed 3 hours on update", (done) ->
    data.name = "garage170"
    garage_service.createGarage data, (err, garage) ->
      garage.event_buffer.start = 3
      garage.event_buffer.stop = 4
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid event post buffer"
        done()

  it "should not allow invalid event pre buffer start format on create", (done) ->
    data.name = "garage171"
    data.event_buffer.start = "hello"
    data.event_buffer.stop = 3
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid event buffer format"
      done()

  it "should not allow invalid event post buffer stop format on create", (done) ->
    data.name = "garage172"
    data.event_buffer.start = 3
    data.event_buffer.stop = "hello"
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid event buffer format"
      done()

  it "should not allow event pre buffer to be negative on create", (done) ->
    data.name = "garage173"
    data.event_buffer.start = -1
    data.event_buffer.stop = 3
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid event pre buffer"
      done()

  it "should not allow event post buffer to be negative on create", (done) ->
    data.name = "garage174"
    data.event_buffer.start = 3
    data.event_buffer.stop = -1
    garage_service.createGarage data, (err, garage) ->
      should.exist err
      err.should.equal "invalid event post buffer"
      done()

  it "should not allow event pre buffer to be negative on update", (done) ->
    data.name = "garage175"
    garage_service.createGarage data, (err, garage) ->
      garage.event_buffer.start = -1
      garage.event_buffer.stop = 3
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid event pre buffer"
        done()

  it "should not allow event post buffer to be negative on update", (done) ->
    data.name = "garage176"
    garage_service.createGarage data, (err, garage) ->
      garage.event_buffer.start = 3
      garage.event_buffer.stop = -1
      garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
        should.exist err
        err.should.equal "invalid event post buffer"
        done()

  # it "should not allow invalid event pre buffer start format on update", (done) ->
  #   data.name = "garage173"
  #   garage_service.createGarage data, (err, garage) ->
  #     garage.event_buffer.start = "hello"
  #     garage.event_buffer.stop = 3
  #     garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
  #       should.exist err
  #       err.should.equal "invalid event pre buffer"
  #       done()

  # it "should not allow invalid event post buffer stop format on update", (done) ->
  #   data.name = "garage174"
  #   garage_service.createGarage data, (err, garage) ->
  #     garage.event_buffer.start = 3
  #     garage.event_buffer.stop = "hello"
  #     garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
  #       should.exist err
  #       err.should.equal "invalid event post buffer"
  #       done()

  # it "does not create a garage when operating closed hour is earlier than operating open hour", (done) ->
  #   data.name = "garage16"
  #   data.hours.operating.open = "03:00"
  #   data.hours.operating.closed = "01:59"
  #   garage_service.createGarage data, (err, garage) ->
  #     should.exist err
  #     err.should.equal "operating 'closed' hour is earlier than 'open' hour"
  #     done()

  # it "does not update a garage without operating start hour value", (done) ->
  #   data.name = "garage18"
  #   garage_service.createGarage data, (err, garage) ->
  #     garage.hours.operating.open = null
  #     garage_service.updateGarage garage.id, garage, (err, garage) ->
  #       should.exist err
  #       err.should.equal "'open' operating hour value required"
  #       done()

  # it "does not update a garage without operating stop hour value", (done) ->
  #   data.name = "garage19"
  #   garage_service.createGarage data, (err, garage) ->
  #     garage.hours.operating.closed = null
  #     garage_service.updateGarage garage.id, garage, (err, garage) ->
  #       should.exist err
  #       err.should.equal "'closed' operating hour value required"
  #       done()

  # it "does not update a garage without operating start hour key", (done) ->
  #   data.name = "garage20"
  #   garage_service.createGarage data, (err, garage) ->
  #     garage = _.defaults garage, {}
  #     garage.hours.operating.open = undefined
  #     garage_service.updateGarage garage.id, garage, (err, garage) ->
  #       should.exist err
  #       err.should.equal "'open' operating hour key required"
  #       done()

  # it "does not update a garage without operating stop hour key", (done) ->
  #   data.name = "garage21"
  #   garage_service.createGarage data, (err, garage) ->
  #     garage.hours.operating.closed = undefined
  #     garage_service.updateGarage garage.id, garage, (err, garage) ->
  #       should.exist err
  #       err.should.equal "'closed' operating hour key required"
  #       done()

  # it "does not update a garage without operating hours object", (done) ->
  #   data.name = "garage22"
  #   garage_service.createGarage data, (err, garage) ->
  #     garage.hours.operating = undefined
  #     garage_service.updateGarage garage.id, garage, (err, garage) ->
  #       should.exist err
  #       err.should.equal "'open' operating hour key required"
  #       done()

  # it "does not update a garage when operating closed hour is earlier than operating open hour", (done) ->
  #   data.name = "garage32"
  #   garage_service.createGarage data, (err, garage) ->
  #     garage.hours.operating.open = "03:00"
  #     garage.hours.operating.closed = "01:59"
  #     garage_service.updateGarage garage.id, garage, (err, garage) ->
  #       should.exist err
  #       err.should.equal "operating 'closed' hour is earlier than 'open' hour"
  #       done()

  # it "does not create a garage w/ null number of spaces", (done) ->
  #   data.name = "garage48"
  #   data.num_spaces = null
  #   garage_service.createGarage data, (err, garage) ->
  #     should.exist err
  #     err.should.equal "invalid number of spaces"
  #     done()

  # it "does not update a garage w/ null number of spaces", (done) ->
  #   data.name = "garage50"
  #   garage_service.createGarage data, (err, garage) ->
  #     garage.num_spaces = null
  #     garage_service.updateGarage garage.id, garage, (err, garage) ->
  #       should.exist err
  #       err.should.equal "invalid number of spaces"
  #       done()

  # it "should not create a garage when operating open and closed are equal", (done) ->
  #   data.name = "garage65"
  #   data.hours.operating.open = "16:07"
  #   data.hours.operating.closed = "16:07"
  #   garage_service.createGarage data, (err, garage) ->
  #     should.exist err
  #     err.should.equal "invalid operating hour duration"
  #     done()

  # it "should not update a garage when operating open and closed are equal", (done) ->
  #   data.name = "garage66"
  #   garage_service.createGarage data, (err, garage) ->
  #     garage.hours.operating.open = "16:07"
  #     garage.hours.operating.closed = "16:07"
  #     garage_service.updateGarage garage.id, garage, (err, updatedGarage) ->
  #       should.exist err
  #       err.should.equal "invalid operating hour duration"
  #       done()

  # # it "should associate region on create", (done) ->
  # #   done("not implemented")

  # # it "should associate region on update", (done) ->
  # #   done("not implemented")

  # # it "should allow height to be null when garage type is 'surface' on create", (done) ->
  # #   done("not implemented")

  # # it "should not allow height to be null when garage type is 'above ground' on create", (done) ->
  # #   done("not implemented")

  # # it "should not allow height to be null when garage type is 'below ground' on create", (done) ->
  # #   done("not implemented")

  # # it "should allow height to be null when garage type is 'surface' on update", (done) ->
  # #   done("not implemented")

  # # it "should not allow height to be null when garage type is 'above ground' on update", (done) ->
  # #   done("not implemented")

  # # it "should not allow height to be null when garage type is 'below ground' on update", (done) ->
  # #   done("not implemented")

  # it "should reject rates that exceed operational hour bounds", (done) ->
  #   done("not implemented")

  # it "does not create a garage without operating start hour value", (done) ->
  #   data.name = "garage2"
  #   data.hours.operating.open = null
  #   garage_service.createGarage data, (err, garage) ->
  #     should.exist err
  #     err.should.equal "'open' operating hour value required"
  #     done()

  # it "does not create a garage without operating stop hour value", (done) ->
  #   data.name = "garage3"
  #   data.hours.operating.closed = null
  #   garage_service.createGarage data, (err, garage) ->
  #     should.exist err
  #     err.should.equal "'closed' operating hour value required"
  #     done()

  # it "does not create a garage without operating start hour key", (done) ->
  #   data.name = "garage4"
  #   data.hours.operating.open = undefined
  #   garage_service.createGarage data, (err, garage) ->
  #     should.exist err
  #     err.should.equal "'open' operating hour key required"
  #     done()

  # it "does not create a garage without operating stop hour key", (done) ->
  #   data.name = "garage5"
  #   data.hours.operating.closed = undefined
  #   garage_service.createGarage data, (err, garage) ->
  #     should.exist err
  #     err.should.equal "'closed' operating hour key required"
  #     done()

  # it "does not create a garage without operating hours object", (done) ->
  #   data.name = "garage6"
  #   data.hours.operating = undefined
  #   garage_service.createGarage data, (err, garage) ->
  #     should.exist err
  #     err.should.equal "operating hours required"
  #     done()