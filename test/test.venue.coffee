config = require '../config/config'
mongoose = require 'mongoose'
_ = require "lodash"

require('../models')()
venue_service = require '../services/service.venue'

should = require('chai').should()

describe "venue service", () ->

  data = null

  before (done) ->
    mongoose.connect config.DBURLTEST, () ->
      mongoose.connection.db.dropDatabase()
      done()

  beforeEach (done) -> 
    venue =
      active: true
      name: _.uniqueId("venue")
      phone: "123-123-1234"
      web: "admin@veerapp.com"
      geo: [ -71.074722, 42.349167 ]
      address: [
        street: "12 Atlantic Ave"
        city: "Boston"
        state: "MA"
        zip: "02189"
      ]
    data = venue
    done()

  it "does not create a venue with null name create", (done) ->
    data.name = null
    venue_service.createVenue data, (err, user) ->
      should.exist err
      err.should.equal "invalid venue name"
      done()

  it "does not create a venue with empty name create", (done) ->
    data.name = ""
    venue_service.createVenue data, (err, user) ->
      should.exist err
      err.should.equal "invalid venue name"
      done()

  it "does not create a venue with undefined name create", (done) ->
    data.name = undefined
    venue_service.createVenue data, (err, user) ->
      should.exist err
      err.should.equal "invalid venue name"
      done()

  it "does not create a venue with null name update", (done) ->
    venue_service.createVenue data, (err, venue) ->
      should.not.exist err
      venue.name = null
      venue_service.updateVenue venue.id, venue, (err, venue) ->
        should.exist err
        err.should.equal "invalid venue name"
        done()

  it "does not create a venue with empty name update", (done) ->
    venue_service.createVenue data, (err, venue) ->
      should.not.exist err
      venue.name = ""
      venue_service.updateVenue venue.id, venue, (err, venue) ->
        should.exist err
        err.should.equal "invalid venue name"
        done()

  it "does not create a venue with null name update", (done) ->
    venue_service.createVenue data, (err, venue) ->
      should.not.exist err
      venue.name = undefined
      venue_service.updateVenue venue.id, venue, (err, venue) ->
        should.exist err
        err.should.equal "invalid venue name"
        done()

  it "does not create a venue with invalid email on create", (done) ->
    data.email = "admin@ddd"
    venue_service.createVenue data, (err, user) ->
      should.exist err
      err.should.equal "invalid email address"
      done()

  it "does not create a venue with invalid email on update", (done) ->
    venue_service.createVenue data, (err, venue) ->
      should.not.exist err
      venue.email = "admin@ddd"
      venue_service.updateVenue venue.id, venue, (err, venue) ->
        should.exist err
        err.should.equal "invalid email address"
        done() 
