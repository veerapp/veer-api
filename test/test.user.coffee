config = require '../config/config'
mongoose = require 'mongoose'
_ = require "lodash"

require('../models')()
user_service = require '../services/service.user'

should = require('chai').should()

describe "user service", () ->

  data = null

  before (done) -> 
    mongoose.connect config.DBURLTEST, () ->
      mongoose.connection.db.dropDatabase()
      done()

  beforeEach (done) ->
    user = 
      email: _.uniqueId("admin") + "@veerapp.com"
      password: "hello_veer"
      phone: "123-123-1234"
    data = user
    done()

  it "creates a new user via email & password", (done) ->
    user_service.createUser data, (err, user) ->
      should.not.exist err
      should.exist user
      user.should.be.an 'object'
      user.email.should.equal data.email
      done()

  # # it "create a new user via facebook", (done) ->
  # #   done("not implemented")

  it "retrieves by id", (done) ->
    user_service.createUser data, (err, user) ->
      user_service.getUser user.id, (err, user) ->
        should.not.exist err
        should.exist user
        user.should.be.an 'object'
        user.id.should.equal user.id
        done()

  # # it "retrieves them all", (done) ->
  # #   user_service.getUsers {}, (err, users) ->
  # #     should.not.exist err
  # #     should.exist users
  # #     users.should.be.an('array').with.length(1)
  # #     done()

  it "should update existing user's phone", (done) ->
    user_service.createUser data, (err, user) ->
      should.not.exist err
      should.exist user
      user.phone = "508-123-1234"
      user_service.updateUser user.id, user, (err, updated_user) ->
        should.not.exist err
        should.exist updated_user
        updated_user.email.should.equal updated_user.email
        updated_user.phone.should.equal user.phone
        done()

  it "deletes a user", (done) ->
    user_service.createUser data, (err, user) ->
      should.not.exist err
      should.exist user
      USER_ID = user.id
      user_service.deleteUser user.id, (err, user) ->
        should.not.exist err
        should.exist user
        user.should.be.an 'object'
        user.id.should.equal USER_ID
        user_service.getUser USER_ID, (err, user) ->
          should.not.exist user
          done()

  it "should not create a user without an email", (done) ->
    data.email = null
    user_service.createUser data, (err, user) ->
      should.exist err
      err.should.equal "email required"
      done()

  it "should not create a user without a password", (done) ->
    data.password = null
    user_service.createUser data, (err, user) ->
      should.exist err
      err.should.equal "password required"
      done()

  it "should not create a user with no email or password", (done) ->
    data.email = null
    data.password = null
    user_service.createUser data, (err, user) ->
      should.exist err
      err.should.equal "email required"
      done()

  it "should trim email address on create", (done) ->
    EMAIL = data.email
    data.email = " " + EMAIL + " "
    user_service.createUser data, (err, user) ->
      should.not.exist err
      should.exist user
      user.should.be.an 'object'
      user.email.should.equal EMAIL
      user.phone.should.equal data.phone
      done()

  it "should trim phone number on create", (done) ->
    PHONE = data.phone
    data.phone = " " + PHONE + " "
    user_service.createUser data, (err, user) ->
      should.not.exist err
      should.exist user
      user.should.be.an 'object'
      user.email.should.equal data.email
      user.phone.should.equal "123-123-1234"
      done()

  it "should trim email address on update", (done) ->
    user_service.createUser data, (err, user) ->
      should.not.exist err
      should.exist user
      EMAIL = user.email
      user.email =  " " + EMAIL + " "
      user_service.updateUser user.id, user, (err, updated_user) ->
        should.not.exist err
        should.exist updated_user
        updated_user.email.should.equal EMAIL
        updated_user.phone.should.equal data.phone
        done()

  it "should trim phone number on update", (done) ->
    user_service.createUser data, (err, user) ->
      should.not.exist err
      should.exist user
      PHONE = user.phone
      user.phone = " " + PHONE + " "
      user_service.updateUser user.id, user, (err, updated_user) ->
        should.not.exist err
        should.exist updated_user
        updated_user.email.should.equal data.email
        updated_user.phone.should.equal PHONE
        done()

  it "should not create user with invalid email", (done) ->
    data.email = "invalid"
    user_service.createUser data, (err, user) ->
      should.exist err
      err.should.equal "invalid email address"
      done()
