# [Veer](https://www.veerapp.com)

The Veer API is the backend implementation of the Veer mobile product.

== Overview ==

Veer is a mobile application that finds the closest and cheapest parking spot

== Setup ==

- Install prerequisites
    - npm install -g coffee-script
    - npm install -g gulp

- Install dependencies:
    - npm install

- Run the server
    - npm start || coffee server.coffee

- Run the tests
    - npm test

- Run gulp
    - npm run gulp (for dev)
    - npm run gulp-build (for prod)