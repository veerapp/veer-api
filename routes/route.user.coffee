logger = require '../config/logger'
auth = require '../config/auth'

user_service = require '../services/service.user'
underscore = require 'underscore'

module.exports = (app) ->

  app.post '/api/1/users/auth', [ auth.none ], (req, res) ->
    user_service.authenticateUser req.body, (err, user) ->
      return res.send 400, err if err
      res.json 201, user

  # app.get '/api/1/users/auth/facebook', [ auth.facebook ], (req, res) ->

  app.get '/api/1/users', [ auth.basic ], (req, res) ->
    user_service.getUsers req.query, (err, users) ->
      return res.send 400, err if err
      res.json 200, users

  app.get '/api/1/users/:user_id', [ auth.basic ], (req, res) ->
    user_service.getUser req.params.user_id, (err, user) ->
      return res.send 400, err if err
      res.json 200, user

  app.post '/api/1/users', [ auth.none ], (req, res) ->
    user_service.createUser req.body, (err, user) ->
      return res.send 400, err if err
      res.json 201, user

  app.del '/api/1/users/:user_id', [ auth.basic ], (req, res) ->
    startup_service.deleteUser req.params.user_id, (err, startup) ->
      return res.send 400, err if err
      res.json 200, startup
