logger = require '../config/logger'
auth = require '../config/auth'

garage_service = require '../services/service.garage'
underscore = require 'underscore'

module.exports = (app) ->

  app.get '/api/1/garages', [ auth.basic ], (req, res) ->
    garage_service.getGarages req.query, (err, garages) ->
      return res.send 400, err if err
      res.json 200, garages

  app.get '/api/1/garages/:garage_id', [ auth.basic ], (req, res) ->
    garage_service.getGarage req.params.garage_id, (err, user) ->
      return res.send 400, err if err
      res.json 200, user

  app.put '/api/1/garages/:garage_id', [ auth.basic ], (req, res) ->
    garage_service.updateGarage req.params.garage_id, req.body, (err, user) ->
      return res.send 400, err if err
      res.json 200, user

  app.post '/api/1/garages', [ auth.basic ], (req, res) ->
    garage_service.createGarage req.body, (err, user) ->
      return res.send 400, err if err
      res.json 201, user

  app.del '/api/1/garages/:garage_id', [ auth.basic ], (req, res) ->
    garage_service.deleteGarage req.params.garage_id, (err, startup) ->
      return res.send 400, err if err
      res.json 200, startup
