logger = require '../config/logger'
auth = require '../config/auth'

operator_service = require '../services/service.operator'
underscore = require 'underscore'

module.exports = (app) ->

  app.get '/api/1/operators', [ auth.basic ], (req, res) ->
    operator_service.getOperators req.query, (err, garages) ->
      return res.send 400, err if err
      res.json 200, garages

  app.get '/api/1/operators/:operator_id', [ auth.basic ], (req, res) ->
    operator_service.getOperator req.params.operator_id, (err, user) ->
      return res.send 400, err if err
      res.json 200, user

  app.put '/api/1/operators/:operator_id', [ auth.basic ], (req, res) ->
    operator_service.updateOperator req.params.operator_id, req.body, (err, user) ->
      return res.send 400, err if err
      res.json 200, user

  app.post '/api/1/operators', [ auth.basic ], (req, res) ->
    operator_service.createOperator req.body, (err, user) ->
      return res.send 400, err if err
      res.json 201, user

  app.del '/api/1/operators/:operator_id', [ auth.basic ], (req, res) ->
    operator_service.deleteOperator req.params.operator_id, (err, startup) ->
      return res.send 400, err if err
      res.json 200, startup
