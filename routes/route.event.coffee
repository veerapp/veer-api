logger = require '../config/logger'
auth = require '../config/auth'

event_service = require '../services/service.event'
underscore = require 'underscore'

module.exports = (app) ->

  app.get '/api/1/events', [ auth.basic ], (req, res) ->
    event_service.getGarages req.query, (err, garages) ->
      return res.send 400, err if err
      res.json 200, garages

  app.get '/api/1/events/:event_id', [ auth.basic ], (req, res) ->
    event_service.getGarage req.params.event_id, (err, user) ->
      return res.send 400, err if err
      res.json 200, user

  app.post '/api/1/events', [ auth.basic ], (req, res) ->
    event_service.createGarage req.body, (err, user) ->
      return res.send 400, err if err
      res.json 201, user

  app.del '/api/1/events/:event_id', [ auth.basic ], (req, res) ->
    event_service.deleteGarage req.params.event_id, (err, startup) ->
      return res.send 400, err if err
      res.json 200, startup
