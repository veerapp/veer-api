logger = require '../config/logger'
auth = require '../config/auth'

gridfs_service = require '../services/service.gridfs'
underscore = require 'underscore'

mongoose = require "mongoose"
# Busboy = require "busboy"
Grid = require "gridfs-stream"
GFS = Grid(mongoose.connection.db, mongoose.mongo)

module.exports = (app) ->

  app.get '/api/1/files/:file_id', [ auth.none ], (req, res) ->
    gridfs_service.getFile req.params.file_id, res, (err, file) ->
      return res.send 400, err if err
      res.json 200, file

  app.put '/api/1/files/:file_id', [ auth.basic ], (req, res) ->
    gridfs_service.updateFile req.params.file_id, req, (err, file) ->
      return res.send 400, err if err
      res.json 200, file

  app.post '/api/1/files', [ auth.basic ], (req, res) ->
    gridfs_service.createFile req, (err, file) ->
      return res.send 400, err if err
      res.json 201, file

  app.del '/api/1/files/:file_id', [ auth.basic ], (req, res) ->
    gridfs_service.deleteFile req.params.file_id, (err, file) ->
      return res.send 400, err if err
      res.json 200, file
