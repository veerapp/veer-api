logger = require '../config/logger'
auth = require '../config/auth'

venue_service = require '../services/service.venue'
underscore = require 'underscore'

module.exports = (app) ->

  app.get '/api/1/venues', [ auth.basic ], (req, res) ->
    venue_service.getVenues req.query, (err, venues) ->
      return res.send 400, err if err
      res.json 200, venues

  app.get '/api/1/venues/:venue_id', [ auth.basic ], (req, res) ->
    venue_service.getVenue req.params.venue_id, (err, user) ->
      return res.send 400, err if err
      res.json 200, user

  app.put '/api/1/venues/:venue_id', [ auth.basic ], (req, res) ->
    venue_service.updateVenue req.params.venue_id, req.body, (err, user) ->
      return res.send 400, err if err
      res.json 200, user

  app.post '/api/1/venues', [ auth.basic ], (req, res) ->
    venue_service.createVenue req.body, (err, user) ->
      return res.send 400, err if err
      res.json 201, user

  app.del '/api/1/venues/:venue_id', [ auth.basic ], (req, res) ->
    venue_service.deleteVenue req.params.venue_id, (err, startup) ->
      return res.send 400, err if err
      res.json 200, startup
