passport = require("passport")

module.exports =

  none: (req, res, next) -> next()

  basic_admin: passport.authenticate('basic-admin', {session: false})

  basic: passport.authenticate('basic', { session: false })

  facebook: passport.authenticate('facebook', { session: false })

  anonymous: passport.authenticate('anonymous', { session: false })






