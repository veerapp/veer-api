mongoose = require 'mongoose'
BasicStrategy = require('passport-http').BasicStrategy
FacebookStrategy = require('passport-facebook').Strategy
AnonymousStrategy = require('passport-anonymous').Strategy
User = require("../models/user")
logger = require "./logger"
config = require './config'

module.exports = (passport) ->

  passport.use new BasicStrategy ({}), (email, password, done) ->
    logger.info "validating user credentials [" + email + ", " + password + "]"
    User.model.findOne { email: email }, (err, user) ->
      if err then return done(err)
      if not user then return done("invalid credentials", false)
      if user.password != password then return done("invalid credentials", false)
      # if not user.authenticate(password) then return done(null, false)
      return done(null, user)

  passport.use new FacebookStrategy(
    clientID: config.FACEBOOK_APP_ID
    clientSecret: config.FACEBOOK_APP_SECRET
    callbackURL: "http://127.0.0.1:3000/auth/facebook/callback"
  , (accessToken, refreshToken, profile, done) ->
    User.findOrCreateFacebookUser
      "facebook.id": profile.id
    , (err, user) ->
      done err, user
  )

  passport.use new AnonymousStrategy()

  passport.serializeUser (user, done) ->
    done(null, user)

  passport.deserializeUser (user, done) ->
    done(null, user)