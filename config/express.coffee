path = require 'path'
config = require './config'
express = require 'express'

# express midlewares
helmet = require 'helmet'
session = require 'express-session'
sessionStore = require('connect-mongo')({session: session})
compression = require 'compression'
bodyParser = require 'body-parser'
cookieParser = require 'cookie-parser'
methodOverride = require 'method-override'
errorHandler = require 'errorhandler'
serveStatic = require 'serve-static'
busboy = require 'connect-busboy'

module.exports = (passport, db, logger, root_path) ->

  app = express()

  app.logger = logger

  # set port, routes, models and config paths
  app.set 'port', config.PORT
  app.set 'routes', root_path + '/routes/'
  # app.set 'models', root_path + '/data_access/models/'
  app.set 'config', config

  # security headers
  app.use helmet.xframe()
  app.use helmet.iexss()
  app.use helmet.contentTypeOptions()
  app.use helmet.cacheControl()

  # ensure all assets and data are compressed - above static
  app.use compression(level: 9)

  # setting the favicon and static folder
  # app.use favicon path.join root_path, '/_public/assets/favicon.ico'
  app.use serveStatic path.join root_path, '/public'

  # body parsing middleware - above methodOverride()
  app.use bodyParser.json()
  app.use bodyParser.urlencoded()
  app.use express.query()
  app.use methodOverride()
  app.use busboy()

  # app.engine 'html', require('ejs').renderFile
  # app.set 'view engine', 'html'

  # initialize passport
  app.use passport.initialize()

  # bootstrap routes
  require("../routes")(app)

  app.use (err, req, res, next) ->
    logger.error err.toString()
    next()

  if config.ENV is 'development' then  app.use errorHandler { dumpExceptions: true, showStack: true }

  return app

