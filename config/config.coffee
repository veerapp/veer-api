base =
  ENV: process.env.NODE_ENV || 'development'
  PORT: process.env.PORT || 3000
  VERSION: 1
  LOGPATH: "veer.log"
  COOKIE_SECRET: "thisisthesecretforthesession"
  DBURLTEST: "mongodb://localhost/veer_test"

dev =
  DBURL: "mongodb://localhost/veer_dev"
  FACEBOOK_APP_ID: "1513092282238511"
  FACEBOOK_APP_SECRET: "fce57b96b31bde523410fe8a336cc654"
  TWITTER_CONSUMER_KEY: "UHTBgNZcRezk4YqZEWg7X11qi"
  TWITTER_CONSUMER_SECRET: "4GXVeuaqEeGze640duy7xe0fFFnfbKN5WIvz25BYp5ZM3wW4X9"

test =
  DBURL: "mongodb://localhost/veer_test"
  FACEBOOK_APP_ID: "1513092282238511"
  FACEBOOK_APP_SECRET: "fce57b96b31bde523410fe8a336cc654"
  TWITTER_CONSUMER_KEY: "UHTBgNZcRezk4YqZEWg7X11qi"
  TWITTER_CONSUMER_SECRET: "4GXVeuaqEeGze640duy7xe0fFFnfbKN5WIvz25BYp5ZM3wW4X9"

prod =
  DBURL: "mongodb://localhost/veer"
  FACEBOOK_APP_ID: "--insert-facebook-app-id-here--"
  FACEBOOK_APP_SECRET: "--insert-facebook-app-secret-here--"
  TWITTER_CONSUMER_KEY: "--insert-twitter-consumer-key-here--"
  TWITTER_CONSUMER_SECRET: "--insert-twitter-consumer-secret-here--"

mergeConfig = (config) ->
  for key, val of config
    base[key] = val
  base

module.exports = ( ->
  switch base.ENV
    when 'development' then return mergeConfig(dev)
    when 'test' then return mergeConfig(test)
    when 'production' then return mergeConfig(prod)
    else return mergeConfig(dev)
)()