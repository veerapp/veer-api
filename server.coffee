# monitoring with nodetime
if process.env.NODE_ENV == 'production'
  require('nodetime').profile(
    accountKey: "ENTER-A-VALID-KEY-HERE"
    appName: 'veer.coffee'
  )

# dependencies
config = require './config/config'
logger = require './config/logger'
express = require 'express'
mongoose = require 'mongoose'
passport = require 'passport'
_ = require 'lodash'

# catch all uncaught exceptions
process.on 'uncaughtException', (err) ->
  logger.error 'Something very bad happened: ', err.message
  logger.error err.stack
  process.exit 1  # because now, you are in unpredictable state!

# watch and log any leak (a lot of false positive though)
memwatch = require 'memwatch'
memwatch.on 'leak', (d) -> logger.error "LEAK: #{JSON.stringify(d)}"

# bootstap db connection
db = mongoose.connect config.DBURL
logger.info "mongo connected to", config.DBURL

# exit on db connection error
mongoose.connection.on 'error', (err) ->
  logger.error "mongodb error: #{err}"
  process.exit 1

# retry 10 times on db connection lost
attempt = 1
mongoose.connection.on 'disconnected', () ->
  if attempt < 10
    logger.error "mongodb disconnected, trying to reconnect.."
    logger.info "mongodb reconnect, attempt num #{attempt}"
    attempt += 1
    db = mongoose.connect config.DBURL #, opts
  else
    logger.error "mongodb disconnect, giving up!"

# bootstrap models
require('./models')()

# bootstrap passport config
require('./config/passport')(passport)

# express configuration
app = require("./config/express")(passport, db, logger, __dirname)

# bootstrap routes
require("./routes")(app)

# load garage fixture
# garagesJSON = require("./fixtures/garages.json")
# garage_service = require("./services/service.garage")
# _.each garagesJSON, (ingest_garage) ->
#   garage_service.createGarage ingest_garage, (err, garage) ->
#     console.log "created garage: " + garage.name

# start the app
app.listen app.get('port'), ->
  logger.info "veer-api server listening on port #{@address().port} in #{config.ENV} mode"

module.exports = app

